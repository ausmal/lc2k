/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`ifndef DEFS
`define DEFS

`define ADD 4'b0000
`define NAND 4'b0001
`define LW 4'b0010
`define SW 4'b0011
`define BEQ 4'b0100
`define JALR 4'b0101
`define HALT 4'b0110
`define NOOP 4'b0111
`define OR 4'b1000
`define AND 4'b1001
`define LWL 4'b1010
`define SWC 4'b1011
`define NOT 4'b1100
`define SL 4'b1101
`define SR 4'b1110
`define SUB 4'b1111
`define DEBUG


typedef enum logic [2:0] {
                          NONE,
                          BUS_READ_LINE,
                          BUS_WRITE_LINE,
                          BUS_INVALIDATE_LINE,
                          BUS_READ_INVALIDATE_LINE
                          } mem_bus_op_t;

typedef enum logic [1:0] {
                          INVALID,
                          EXCLUSIVE,
                          MODIFIED,
                          SHARED
              } state_t;

`endif
