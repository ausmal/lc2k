/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module memory_bus (
                   input                    clock,
                   input                    reset,
                   
                   input wire [3:0][31:0]   core2mem_address,
                   input wire [3:0][31:0]   core2mem_data,
                   input                    mem_bus_op_t [3:0] core2mem_bus_op,
                   input wire [3:0]         core2mem_start_txn,

                   output logic [31:0]      mem2core_data,
                   output logic [3:0]       mem2core_done,
                   
                   inout wor                core2mem_HIT,
                   inout wor                core2mem_HITM,
                   inout wor [31:0]         core2mem_HITM_data,
                   output logic             bus2core_writeback_done,

                   output logic [31:0]      mem_bus_winner_address,
                   output                   mem_bus_op_t mem_bus_winner_op,

                   input [31:0]             mem2bus_data,
                   input                    mem2bus_done,
                   
                   output logic [31:0]      bus2mem_address,
                   output logic [31:0]      bus2mem_data,
                   output logic             bus2mem_write,
                   output logic             bus2mem_start_txn,

                   output logic [3:0][31:0] mmio_address,
                   output logic [3:0][31:0] mmio_data_in,
                   input [3:0][31:0]        mmio_data_out,
                   output logic [3:0]       mmio_write,
                   input [3:0]              mmio_acknowledged,

                   output logic [31:0]      console_address,
                   output logic [31:0]      console_data_in,
                   input [31:0]             console_data_out,
                   output logic             console_write,
                   input                    console_done

                   );

  logic [1:0]                               old_selected_core;
  logic [1:0]                               selected_core;
  logic [1:0]                               selected_core_valid;
  logic [1:0]                               core_waiting;

  always_ff @(posedge clock) begin
    if (reset) begin
      old_selected_core <= 0;
    end else begin
      old_selected_core <= selected_core;
    end
  end
  
  
  
  /* Pick the core that wins. */
  always_comb begin
    selected_core = 0;
    selected_core_valid = 0;
    core_waiting = 0;
    for (int i = 0; i < 4; i++) begin
      if (core2mem_start_txn[i]) begin
        core_waiting++;
      end
    end

    for (int i = 0; i < 4; i++) begin
      if (core2mem_start_txn[i] && !selected_core_valid) begin
        if (i != old_selected_core || core_waiting < 2) begin
          selected_core = i;
          selected_core_valid = 1;
          $display("Selected core %d", i);
        end
      end
    end

    $display("There were %d cores waiting. We selected %d", core_waiting, selected_core);
  end

  always_comb begin
    mem_bus_winner_address = core2mem_address[selected_core];
    mem_bus_winner_op = core2mem_bus_op[selected_core];
    bus2mem_data = 0;
    bus2mem_address = 0;
    bus2mem_write = 0;
    bus2mem_start_txn = 0;
    bus2core_writeback_done = 0;
    mem2core_data = 0;
    mem2core_done = 0;
    mmio_address = 0;
    mmio_data_in = 0;
    mmio_write = 0;
    console_address = 0;
    console_data_in = 0;
    console_write = 0;
    

    if (core2mem_address[selected_core][31]) begin
      $display("BUS REQUEST!");
      /* This is a bus question. */
      if (core2mem_address[selected_core] == 32'hffff_ffff) begin
        /* Core wants to know its number. */
        mem2core_data = selected_core;
        mem2core_done[selected_core] = 1;
      end
    end else if (core2mem_address[selected_core][30]) begin
      $display("IO REQUEST!");
      /* This is a transaction on the MMIO bus side of things. */
      
      /* Here's where devices get plugged in. */
      

      /* The serial/console controller */
      if (core2mem_address[selected_core][27:16] == 12'h001) begin
        console_address = core2mem_address[selected_core];
        console_data_in = core2mem_data[selected_core];
        mem2core_data = console_data_out;
        console_write = (core2mem_bus_op[selected_core] == BUS_WRITE_LINE);
        mem2core_done[selected_core] = console_done;
      end
      

      /* The other cores */
      if (core2mem_address[selected_core][27:16] == 12'h000) begin
        mmio_address[core2mem_address[selected_core][15:12]] = core2mem_address[selected_core];
        mmio_data_in[core2mem_address[selected_core][15:12]] = core2mem_data[selected_core];
        mem2core_data = mmio_data_out[core2mem_address[selected_core][15:12]];
        mem2core_done[selected_core] = mmio_acknowledged[core2mem_address[selected_core][15:12]];
        mmio_write[core2mem_address[selected_core][15:12]] = (core2mem_bus_op[selected_core] == BUS_WRITE_LINE);
      end
      
      
    end else begin
      /* This is a conventional transaction */
      if (core2mem_bus_op[selected_core] == BUS_READ_LINE ||
          core2mem_bus_op[selected_core] == BUS_INVALIDATE_LINE ||
          core2mem_bus_op[selected_core] == BUS_READ_INVALIDATE_LINE) begin
        if (core2mem_HITM) begin
          bus2mem_data = core2mem_HITM_data;
          bus2mem_address = mem_bus_winner_address;
          bus2mem_write = 1;
          bus2mem_start_txn = 1;
          bus2core_writeback_done = mem2bus_done;
          
          mem2core_data = core2mem_HITM_data;
          mem2core_done[selected_core] = 0; //mem2bus_done;
        end else begin
          bus2mem_address = mem_bus_winner_address;
          bus2mem_start_txn = 1;

          mem2core_data = mem2bus_data;
          mem2core_done[selected_core] = mem2bus_done;
        end
      end else if (core2mem_bus_op[selected_core] == BUS_WRITE_LINE) begin // if (core2mem_bus_op[selected_core] == BUS_READ_LINE)
        bus2mem_data = core2mem_data[selected_core];
        bus2mem_address = core2mem_address[selected_core];
        bus2mem_write = 1;
        bus2mem_start_txn = 1;
        assert(core2mem_HIT == 0 && core2mem_HITM == 0) else begin
          $error("Somebody has this data and we thought we were exclusive!");
        end
      end
    end // else: !if(core2mem_adddress[selected_core][30])
  end // always_comb

endmodule // memory_bus

