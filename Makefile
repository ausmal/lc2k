#This should point at your modelsim installation.
MODELSIM=/home/ausmal/altera/13.1/modelsim_ase

helpers.sl: *.v helpers.c core/*.v
	rm -rf work test_i test_o
	vlib work
	vlog -sv sys_defs.vh core/*.v *.v
#May need to adjust for your machine. (might not need -fPIC or -m32)
	gcc -m32 -c -g -I$(MODELSIM)/include helpers.c -fPIC -lpthread 
	ld -shared -E -o helpers.sl helpers.o -melf_i386
	mkfifo test_i
	mkfifo test_o

dual:		helpers.sl
	vsim -c -do	"run 1000ms; quit" dual_testbench 

quad:		helpers.sl
	vsim -c -do	"run 1000ms; quit" quad_testbench

single:		helpers.sl
	vsim -c -do	"run 1000ms; quit" single_testbench -pli helpers.sl

clean:
	rm -rf work helpers.sl
