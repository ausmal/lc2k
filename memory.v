/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module memory(
              input               clock,
              input               reset,

              output logic [31:0] mem2bus_data,
              output logic        mem2bus_done,

              input [31:0]        bus2mem_address,
              input [31:0]        bus2mem_data,

              input               bus2mem_write,
              input               bus2mem_start_txn
              );

  logic [16383:0][31:0]    memory;

  always @(negedge clock) begin
    if (reset) begin
      memory <= 0;
    end else if (bus2mem_write && bus2mem_start_txn) begin
      memory[bus2mem_address] = bus2mem_data;
    end

  end
  always @(*) begin
    mem2bus_data = memory[bus2mem_address];
    mem2bus_done = bus2mem_start_txn;
  end
   

  
endmodule // memory
