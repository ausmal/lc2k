                lw      0       7       thr1_ptr        //Load the address of thread b
                lw      0       6       core1_PC_reg    //Load the address of core 1's PC register
                sw      6       7       0               //Write thread b address to core 1's PC
                lw      0       7       one             //Load the number 1
                sub     6       7       6               //Compute the address of core 1's state register
                sw      6       7       0               //Start core 2
                lw      0       7       thr2_ptr        //Load the address of thread b
                lw      0       6       core2_PC_reg    //Load the address of core 2's PC register
                sw      6       7       0               //Write thread b address to core 2's PC
                lw      0       7       one             //Load the number 1
                sub     6       7       6               //Compute the address of core 2's state register
                sw      6       7       0               //Start core 2
                lw      0       7       thr3_ptr        //Load the address of thread b
                lw      0       6       core3_PC_reg    //Load the address of core 3's PC register
                sw      6       7       0               //Write thread b address to core 3's PC
                lw      0       7       one             //Load the number 1
                sub     6       7       6               //Compute the address of core 3's state register
                sw      6       7       0               //Start core 3
thread0_base    lw      0       1       one             //Load the number 1 to register 1
                lw      0       7       fifty           //Load the number 50 to register 7
thr0_lock       lwl     0       3       counter         //Load-locked the global counter
                add     1       3       3               //Increment the global counter
                swc     0       3       counter         //Try to store it back
                beq     0       3       thr0_lock       //See if the store worked. If not, try again
                add     1       2       2               //The store worked, so increment local counter
                beq     2       7       out             //If we should stop counting, based on local counter, exit.
                beq     0       0       thr0_lock       //Loop, if not.
thread1_base    lw      0       1       one             //Load the number 1 to register 1
                lw      0       7       fifty           //Load the number 50 to register 7
thr1_lock       lwl     0       3       counter         //Load-locked the global counter
                add     1       3       3               //Increment the global counter
                swc     0       3       counter         //Try to store it back
                beq     0       3       thr1_lock       //See if the store worked. If not, try again
                add     1       2       2               //The store worked, so increment local counter
                beq     2       7       out             //If we should stop counting, based on local counter, exit.
                beq     0       0       thr1_lock       //Loop, if not.
thread2_base    lw      0       1       one             //Load the number 1 to register 1
                lw      0       7       fifty           //Load the number 50 to register 7
thr2_lock       lwl     0       3       counter         //Load-locked the global counter
                add     1       3       3               //Increment the global counter
                swc     0       3       counter         //Try to store it back
                beq     0       3       thr2_lock       //See if the store worked. If not, try again
                add     1       2       2               //The store worked, so increment local counter
                beq     2       7       out             //If we should stop counting, based on local counter, exit.
                beq     0       0       thr2_lock       //Loop, if not.
thread3_base    lw      0       1       one             //Load the number 1 to register 1
                lw      0       7       fifty           //Load the number 50 to register 7
thr3_lock       lwl     0       3       counter         //Load-locked the global counter
                add     1       3       3               //Increment the global counter
                swc     0       3       counter         //Try to store it back
                beq     0       3       thr3_lock       //See if the store worked. If not, try again
                add     1       2       2               //The store worked, so increment local counter
                beq     2       7       out             //If we should stop counting, based on local counter, exit.
                beq     0       0       thr3_lock       //Loop, if not.
out             halt
one             .fill   1
fifty           .fill   50
thr1_ptr        .fill   thread1_base
thr2_ptr        .fill   thread2_base
thr3_ptr        .fill   thread3_base
core1_PC_reg    .fill   0x40001001
core2_PC_reg    .fill   0x40002001
core3_PC_reg    .fill   0x40003001
counter         .fill   0                               //If everything works right, this will == 200, when done.
