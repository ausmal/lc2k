/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
`include "sys_defs.vh"

module single_testbench();

  logic clock;
  logic reset;

always begin
  #5
  clock = ~clock;
end

  logic [3:0][31:0]          core2mem_address;       // From pipe_inst of pipeline.v
  mem_bus_op_t [3:0]        core2mem_bus_op;        // From pipe_inst of pipeline.v
  logic [3:0][31:0]          core2mem_data;          // From pipe_inst of pipeline.v
  logic [3:0]                core2mem_start_txn;     // From pipe_inst of pipeline.v
  logic [3:0]                mem2core_done;          // From memory_bus_inst of memory_bus.v

  logic [3:0]                mmio_acknowledged;      // From memory_bus_inst of memory_bus.v, ...
  logic [3:0] [31:0]         mmio_address;           // From memory_bus_inst of memory_bus.v
  logic [3:0] [31:0]         mmio_data_in;           // From memory_bus_inst of memory_bus.v
  logic [3:0] [31:0]         mmio_data_out;          // From pipe_inst of pipeline.v
  logic [3:0]                mmio_write;             // From memory_bus_inst of memory_bus.v

  logic [31:0]          console_address;        // From memory_bus_inst of memory_bus.v
  logic [31:0]          console_data_in;        // From memory_bus_inst of memory_bus.v
  logic [31:0]          console_data_out;       // From console_inst of console.v
  logic                 console_done;           // From console_inst of console.v
  logic                 console_write;          // From memory_bus_inst of memory_bus.v



  logic [7:0]                 char_out;
  logic [7:0]                 char_in;
  logic                       char_in_valid;
  logic                       char_in_acknowledged;
  logic                       char_out_valid;
  logic                       char_out_acknowledged;


  int char_to_c;
  int char_from_c;

  initial begin
    $display("Calling init");
    $init_input;
  end
  
  always @(negedge clock) begin
    if (reset == 0) begin
      char_in = 0;
      char_in_valid = 0;
      char_out_acknowledged = 0;
      if (char_out_valid) begin
        $display("Got character");
        char_out_acknowledged = 1;
        $display("%%%%<- %c", char_out);
        char_to_c = char_out;
        $get_output;
      end
      $get_input;
      if (char_from_c == -1 || char_from_c == -2) begin
        char_in = 0;
        char_in_valid = 0;
      end else begin
        char_in = char_from_c;
        $display("%%%%-> %c", char_in);
        char_in_valid = 1;
      end
    end // if (reset == 0)
  end
  
  

  
  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  logic                 bus2core_writeback_done;// From memory_bus_inst of memory_bus.v
  logic [31:0]          bus2mem_address;        // From memory_bus_inst of memory_bus.v
  logic [31:0]          bus2mem_data;           // From memory_bus_inst of memory_bus.v
  logic                 bus2mem_start_txn;      // From memory_bus_inst of memory_bus.v
  logic                 bus2mem_write;          // From memory_bus_inst of memory_bus.v
  wor                   core2mem_HIT;           // To/From memory_bus_inst of memory_bus.v, ...
  wor                   core2mem_HITM;          // To/From memory_bus_inst of memory_bus.v, ...
  wor [31:0]            core2mem_HITM_data;     // To/From memory_bus_inst of memory_bus.v, ...
  logic [31:0]          mem2bus_data;           // From mem_inst of memory.v
  logic                 mem2bus_done;           // From mem_inst of memory.v
  logic [31:0]          mem2core_data;          // From memory_bus_inst of memory_bus.v
  logic [31:0]          mem_bus_winner_address; // From memory_bus_inst of memory_bus.v
  mem_bus_op_t          mem_bus_winner_op;      // From memory_bus_inst of memory_bus.v
  
  logic                 system_halt;            // From pipe_inst of pipeline.v
  // End of automatics

  logic                 mem_reset;

  assign core2mem_start_txn[3:1] = 0;


console console_inst(/*AUTOINST*/
                       // Outputs
                       .console_data_out(console_data_out[31:0]),
                       .console_done    (console_done),
                       .char_in_acknowledged(char_in_acknowledged),
                       .char_out        (char_out[7:0]),
                       .char_out_valid  (char_out_valid),
                       // Inputs
                       .clock           (clock),
                       .reset           (reset),
                       .console_address (console_address[31:0]),
                       .console_data_in (console_data_in[31:0]),
                       .console_write   (console_write),
                       .char_in         (char_in[7:0]),
                       .char_in_valid   (char_in_valid),
                       .char_out_acknowledged(char_out_acknowledged));
  
  memory mem_inst(/*AUTOINST*/
                  // Outputs
                  .mem2bus_data         (mem2bus_data[31:0]),
                  .mem2bus_done         (mem2bus_done),
                  // Inputs
                  .clock                (clock),
                  .reset                (mem_reset),
                  .bus2mem_address      (bus2mem_address[31:0]),
                  .bus2mem_data         (bus2mem_data[31:0]),
                  .bus2mem_write        (bus2mem_write),
                  .bus2mem_start_txn    (bus2mem_start_txn));
  memory_bus memory_bus_inst(/*AUTOINST*/
                             // Outputs
                             .mem2core_data     (mem2core_data[31:0]),
                             .mem2core_done     (mem2core_done[3:0]),
                             .bus2core_writeback_done(bus2core_writeback_done),
                             .mem_bus_winner_address(mem_bus_winner_address[31:0]),
                             .mem_bus_winner_op (mem_bus_winner_op),
                             .bus2mem_address   (bus2mem_address[31:0]),
                             .bus2mem_data      (bus2mem_data[31:0]),
                             .bus2mem_write     (bus2mem_write),
                             .bus2mem_start_txn (bus2mem_start_txn),
                             .mmio_address      (mmio_address/*[3:0][31:0]*/),
                             .mmio_data_in      (mmio_data_in/*[3:0][31:0]*/),
                             .mmio_write        (mmio_write[3:0]),
                             .mmio_acknowledged (mmio_acknowledged/*[3:0][31:0]*/),
                             // Inouts
                             .core2mem_HIT      (core2mem_HIT),
                             .core2mem_HITM     (core2mem_HITM),
                             .core2mem_HITM_data(core2mem_HITM_data[31:0]),
                             // Inputs
                             .clock	(clock),
                             .reset (reset),
                             .core2mem_address  (core2mem_address/*[3:0][31:0]*/),
                             .core2mem_data     (core2mem_data/*[3:0][31:0]*/),
                             .core2mem_bus_op   (core2mem_bus_op[3:0]),
                             .core2mem_start_txn(core2mem_start_txn[3:0]),
                             .mem2bus_data      (mem2bus_data[31:0]),
                             .mem2bus_done      (mem2bus_done),
                             .mmio_data_out     (mmio_data_out/*[3:0][31:0]*/),

                             .console_address   (console_address[31:0]),
                             .console_data_in   (console_data_in[31:0]),
                             .console_write     (console_write),


                             .console_data_out  (console_data_out[31:0]),
                             .console_done      (console_done));
  pipeline pipe_inst(/*AUTOINST*/
                     // Outputs
                     .core2mem_address  (core2mem_address[0]),
                     .core2mem_data     (core2mem_data[0]),
                     .core2mem_bus_op   (core2mem_bus_op[0]),
                     .core2mem_start_txn(core2mem_start_txn[0]),
                     .system_halt       (system_halt),
                     .mmio_data_out     (mmio_data_out[0]),
                     .mmio_acknowledged (mmio_acknowledged[0]),
                     // Inouts
                     .core2mem_HIT      (core2mem_HIT),
                     .core2mem_HITM     (core2mem_HITM),
                     .core2mem_HITM_data(core2mem_HITM_data[31:0]),
                     // Inputs
                     .clock             (clock),
                     .reset             (reset),
                     .mem2core_data     (mem2core_data[31:0]),
                     .mem2core_done     (mem2core_done[0]),
                     .bus2core_writeback_done(bus2core_writeback_done),
                     .mem_bus_winner_address(mem_bus_winner_address[31:0]),
                     .mem_bus_winner_op (mem_bus_winner_op),
                     .mmio_address      (mmio_address[0]),
                     .mmio_data_in      (mmio_data_in[0]),
                     .mmio_write        (mmio_write[0]),
                     .core_no (0),
                     .core_initial_state(1));
  
  integer               i;
  int                   k;
  integer               file;
  integer               temp;
  int                   showing_data;
  int                   j;
  
initial
  begin
    clock = 0;
    reset = 1;
    mem_reset = 1;
//    $monitor("Time %d, PC: %d, clock %b system_halt %b", $time, pipe_inst.PC, clock, system_halt);

    @(posedge clock);
    @(posedge clock);
    mem_reset = 0;
    $display("Starting to load the memory");
    file = 0;
    file = $fopen("program.mem", "r");
    $display("Got fd %d", file);
    if (file == 0) begin
      $display("We failed to load the memory.");
      $finish_input;
      $finish;
    end


    i = 0;
    while (!$feof(file)) begin
      if ($fscanf(file, "%d", temp)) begin
        mem_inst.memory[i] = temp;
      end
      $display("memory[%d] : %d", i, mem_inst.memory[i]);
      i = i + 1;
    end

    @(posedge clock);
    reset = 0;

    $display("Deasserting reset");

    $display("Enabling core");
    
    $display("Beginning simulation");

    @(posedge clock);
    @(posedge clock);

    while(system_halt != 1) begin
      #10;
    end
    
    @(negedge clock);
    $display("\n\n\n@@@System HALTED on halt instruction\n\n");

    $display("Final contents of register file:");
    for (i = 0; i < 8; i++) begin
      $display("reg[%d] = %d", i, pipe_inst.id_inst.registers_next[i]);
    end
    
    begin
      $display("@@@ Unified Memory contents hex on left, decimal on right: ");
      $display("@@@");
      showing_data = 0;
      for(k=0;k<=16383; k=k+1) begin
        temp = mem_inst.memory[k];
        for (j = 0; j < 32; j++) begin
          if (pipe_inst.dcache_inst.lines_next[j].tag == k &&
              pipe_inst.dcache_inst.lines_next[j].state == 2) begin
            temp = pipe_inst.dcache_inst.lines_next[j].line;
          end
        end
        
        if (temp != 0) begin
          $display("@@@ mem[%5d] = %x : %0d", k, temp, temp);
          showing_data = 1;
        end else if (showing_data != 0) begin
          $display("@@@");
          showing_data = 0;
        end
      end
      $display("@@@");
    end

    $finish_input;
    $finish;
      
   
  end // initial begin

  always_ff @(posedge clock) begin
    $display("POSEDGE");
  end

  always @(negedge clock) begin
    $display("NEGEDGE");
  end

  always @(*) begin
    $monitor("dcache2core_done %d", pipe_inst.dcache2core_done);
    $monitor("dcache2mem_start_txn %d", pipe_inst.dcache2mem_start_txn);
  end
  
  
  always @(negedge clock) begin
    if (!reset) begin
      $display("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      $display("Time is %d\n\n", $time);

      if (pipe_inst.squash) begin
        $display("@!@!@!@!@! BRANCH MISPREDICTION @!@!@!@!@!@!@!@");
      end
      
      
      $display("=========STALLING SIGNALS=========");
      $display("id_stall: %d", pipe_inst.id_stall);
      $display("ex_stall: %d", pipe_inst.ex_stall);
      $display("mem_stall: %d", pipe_inst.mem_stall);
      
      $display("-----------IF STAGE--------------");
      if (pipe_inst.if_instruction_valid) begin
        $display("if_PC: %d", pipe_inst.if_PC);
        $display("if_instruction: %d", pipe_inst.if_instruction);
        $display("if_instruction_valid: %d", pipe_inst.if_instruction_valid);
      end else begin
        $display("IDLE");
      end // else: !if(pipe_inst.if_instruction_valid)
      
      $display("-----------ID STAGE--------------");
      if (pipe_inst.id_valid) begin
        $display("id_PC: %d", pipe_inst.id_PC);
        $display("id_instruction: %d", pipe_inst.id_instruction);
        $display("id_opcode: %d", pipe_inst.id_opcode);
        $display("id_reg_a_idx: %d", pipe_inst.id_reg_a_idx);
        $display("id_reg_a_value: %d", pipe_inst.id_reg_a_value);
        $display("id_reg_a_check: %d", pipe_inst.id_reg_a_check);
        $display("id_reg_b_idx: %d", pipe_inst.id_reg_b_idx);
        $display("id_reg_b_value: %d", pipe_inst.id_reg_b_value);
        $display("id_reg_b_check: %d", pipe_inst.id_reg_b_check);
        $display("id_offset_field: %d", pipe_inst.id_offset_field);
        $display("id_dest_reg: %d", pipe_inst.id_dest_reg);
        $display("id_dest_reg_used: %d", pipe_inst.id_dest_reg_used);
        $display("id_valid: %d", pipe_inst.id_valid);
      end else begin // if (pipe_inst.id_valid)
        $display("IDLE");
      end // else: !if(pipe_inst.id_valid)
      
      $display("-----------EX STAGE--------------");
      if(pipe_inst.ex_valid) begin
        $display("ex_PC: %d", pipe_inst.ex_PC);
        $display("ex_instruction: %d", pipe_inst.ex_instruction);
        $display("ex_opcode: %d", pipe_inst.ex_opcode);
        $display("ex_dest_reg: %d", pipe_inst.ex_dest_reg);
        $display("ex_dest_reg_used: %d", pipe_inst.ex_dest_reg_used);
        $display("ex_dest_value: %d", pipe_inst.ex_dest_value);
        $display("ex_address: %d", pipe_inst.ex_address);
        $display("ex_branch_taken: %d", pipe_inst.ex_branch_taken);
        $display("ex_valid: %d", pipe_inst.ex_valid);
      end else begin // if (pipe_inst.ex_valid)
        $display("IDLE");
      end // else: !if(pipe_inst.ex_valid)
      
      $display("-----------MEM STAGE--------------");
      if(pipe_inst.mem_valid) begin
        $display("mem_PC: %d", pipe_inst.mem_PC);
        $display("mem_instruction: %d", pipe_inst.mem_instruction);
        $display("mem_dest_reg: %d", pipe_inst.mem_dest_reg);
        $display("mem_dest_reg_used: %d", pipe_inst.mem_dest_reg_used);
        $display("mem_dest_value: %d", pipe_inst.mem_dest_value);
        $display("mem_valid: %d", pipe_inst.mem_valid);

        $display("branch_PC_next: %d", pipe_inst.branch_PC_next);
        $display("branch_taken: %d", pipe_inst.branch_taken);
      end else begin // if (pipe_inst.mem_valid)
        $display("IDLE");
      end // else: !if(pipe_inst.mem_valid)
      $display("-----------WB STAGE--------------");
      if(pipe_inst.wb_dest_reg_used) begin
        $display("wb_PC: %d", pipe_inst.wb_PC);
        $display("wb_instruction: %d", pipe_inst.wb_instruction);
        $display("wb_dest_reg: %d", pipe_inst.wb_dest_reg);
        $display("wb_dest_reg_used: %d", pipe_inst.wb_dest_reg_used);
        $display("wb_dest_value: %d", pipe_inst.wb_dest_value);
      end else begin
        $display("IDLE");
      end
      $display("$$$$$$$$$$$$$$CACHES$$$$$$$$$$$$$$$");
      $display("dcache2core_done %b \t dcache2core_data %d", pipe_inst.dcache2core_done, pipe_inst.dcache2core_data);
      $display("icache2core_valid %b \t icache2core_data %d", pipe_inst.icache2core_valid, pipe_inst.icache2core_data);
      $display("PC %d \t icache2mem_start_txn", pipe_inst.PC, pipe_inst.icache2mem_start_txn);


      $display("-------------MEMORY BUS------------------");
      for (i = 0; i < 4; i++) begin
        $display("core2mem_address[%d] %d \t core2mem_data[%d] %d \t core2mem_bus_op[%d] %s, core2mem_start_txn[%d] %d", i, core2mem_address[i], i, core2mem_data[i], i, core2mem_bus_op[i], i, core2mem_start_txn[i]);
      end

      $display("mem2core_data %x | %d", mem2core_data, mem2core_data);
      for (i = 0; i < 4; i++) begin
        $display("mem2core_done[%d] %d", i, mem2core_done[i]);
      end

      $display("HIT: %d \t HITM %d \t HITM_data %d", core2mem_HIT, core2mem_HITM, core2mem_HITM_data);

      $display("mem_bus_winner_address %d \t mem_bus_winner_op %s", mem_bus_winner_address, mem_bus_winner_op);

      $display("mem2bus_data %d \t mem2bus_done %d", mem2bus_data, mem2bus_done);
      $display("bus2mem_address %d \t bus2mem_data %d \t bus2mem_write %d \t bus2mem_start_txn", bus2mem_address, bus2mem_data, bus2mem_write, bus2mem_start_txn);
      
      $display("\n\nRegisters\n");
      for (i = 0; i < 8; i++) begin
        $display("reg[%d] = %d", i, pipe_inst.id_inst.registers_next[i]);
      end


      $display("\n\n\n");
    end // if (!reset)
  end
  
  
endmodule // testbench


// Local Variables:
// verilog-library-flags:("-y verilog/")
// End:
