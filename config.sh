#!/bin/bash

rm -rf work
vlib work
vlog -sv sys_defs.vh core/*.v *.v
vmake > Makefile

sed -i '/whole_library/i \
dual:		whole_library \
	vsim -c -do	"'"run 1000ms; quit"'" dual_testbench \
\
quad:		whole_library \
	vsim -c -do	"'"run 1000ms; quit"'" quad_testbench \
\
single:		whole_library \
	vsim -c -do	"'"run 1000ms; quit"'" single_testbench \
\
' Makefile
