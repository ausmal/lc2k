/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module pipeline(
                input               clock,
                input               reset,

                output logic [31:0] core2mem_address,
                output logic [31:0] core2mem_data,
                output              mem_bus_op_t core2mem_bus_op,
                output logic        core2mem_start_txn,

                input wire [31:0]   mem2core_data,
                input wire          mem2core_done,

                inout wor           core2mem_HIT,
                inout wor           core2mem_HITM,
                inout wor [31:0]    core2mem_HITM_data,
                input logic         bus2core_writeback_done,

                input [31:0]        mem_bus_winner_address,
                input               mem_bus_op_t mem_bus_winner_op,

                output logic        system_halt,

                input [31:0]        mmio_address,
                input [31:0]        mmio_data_in,
                output logic [31:0] mmio_data_out,
                input               mmio_write,
                output logic        mmio_acknowledged,

                input [2:0]         core_no,
                input               core_initial_state
                
                );


  logic                             internal_reset;
  logic                             mmio_reset;

  assign internal_reset = reset || mmio_reset;

  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  logic [31:0]          PC;                     // From pc_inst of pc.v
  logic [31:0]          branch_PC_next;         // From mem_inst of mem_stage.v
  logic                 branch_taken;           // From mem_inst of mem_stage.v
  logic [31:0]          core2dcache_address;    // From mem_inst of mem_stage.v
  logic [31:0]          core2dcache_data;       // From mem_inst of mem_stage.v
  logic                 core2dcache_start_txn;  // From mem_inst of mem_stage.v
  logic                 core2dcache_write;      // From mem_inst of mem_stage.v
  logic [31:0]          dcache2core_data;       // From dcache_inst of dcache.v
  logic                 dcache2core_done;       // From dcache_inst of dcache.v
  logic [31:0]          dcache2mem_address;     // From dcache_inst of dcache.v
  mem_bus_op_t          dcache2mem_bus_op;      // From dcache_inst of dcache.v
  logic [31:0]          dcache2mem_data;        // From dcache_inst of dcache.v
  logic                 dcache2mem_start_txn;   // From dcache_inst of dcache.v
  logic [31:0]          ex_PC;                  // From ex_inst of ex_stage.v
  logic [31:0]          ex_address;             // From ex_inst of ex_stage.v
  logic                 ex_branch_taken;        // From ex_inst of ex_stage.v
  logic [2:0]           ex_dest_reg;            // From ex_inst of ex_stage.v
  logic                 ex_dest_reg_used;       // From ex_inst of ex_stage.v
  logic [31:0]          ex_dest_value;          // From ex_inst of ex_stage.v
  logic [31:0]          ex_instruction;         // From ex_inst of ex_stage.v
  logic [3:0]           ex_opcode;              // From ex_inst of ex_stage.v
  logic                 ex_stall;               // From ex_inst of ex_stage.v
  logic                 ex_valid;               // From ex_inst of ex_stage.v
  logic [31:0]          fetch_PC_next;          // From if_inst of if_stage.v
  logic [31:0]          icache2core_data;       // From icache_inst of icache.v
  logic                 icache2core_valid;      // From icache_inst of icache.v
  logic [31:0]          icache2mem_address;     // From icache_inst of icache.v
  logic                 icache2mem_start_txn;   // From icache_inst of icache.v
  logic [31:0]          id_PC;                  // From id_inst of id_stage.v
  logic [2:0]           id_dest_reg;            // From id_inst of id_stage.v
  logic                 id_dest_reg_used;       // From id_inst of id_stage.v
  logic [31:0]          id_instruction;         // From id_inst of id_stage.v
  logic [31:0]          id_offset_field;        // From id_inst of id_stage.v
  logic [3:0]           id_opcode;              // From id_inst of id_stage.v
  logic                 id_reg_a_check;         // From id_inst of id_stage.v
  logic [2:0]           id_reg_a_idx;           // From id_inst of id_stage.v
  logic [31:0]          id_reg_a_value;         // From id_inst of id_stage.v
  logic                 id_reg_b_check;         // From id_inst of id_stage.v
  logic [2:0]           id_reg_b_idx;           // From id_inst of id_stage.v
  logic [31:0]          id_reg_b_value;         // From id_inst of id_stage.v
  logic                 id_stall;               // From id_inst of id_stage.v
  logic                 id_valid;               // From id_inst of id_stage.v
  logic [31:0]          if_PC;                  // From if_inst of if_stage.v
  logic [31:0]          if_instruction;         // From if_inst of if_stage.v
  logic                 if_instruction_valid;   // From if_inst of if_stage.v
  logic [31:0]          mem2dcache_data;        // From mem_arbitrator_inst of mem_arbitrator.v
  logic                 mem2dcache_done;        // From mem_arbitrator_inst of mem_arbitrator.v
  logic [31:0]          mem2icache_data;        // From mem_arbitrator_inst of mem_arbitrator.v
  logic                 mem2icache_done;        // From mem_arbitrator_inst of mem_arbitrator.v
  logic [31:0]          mem_PC;                 // From mem_inst of mem_stage.v
  logic [2:0]           mem_dest_reg;           // From mem_inst of mem_stage.v
  logic                 mem_dest_reg_used;      // From mem_inst of mem_stage.v
  logic [31:0]          mem_dest_value;         // From mem_inst of mem_stage.v
  logic [31:0]          mem_instruction;        // From mem_inst of mem_stage.v
  logic                 mem_stall;              // From mem_inst of mem_stage.v
  logic                 mem_valid;              // From mem_inst of mem_stage.v
  logic                 squash;                 // From mem_inst of mem_stage.v
  logic [31:0]          wb_PC;                  // From wb_inst of wb_stage.v
  logic [2:0]           wb_dest_reg;            // From wb_inst of wb_stage.v
  logic                 wb_dest_reg_used;       // From wb_inst of wb_stage.v
  logic [31:0]          wb_dest_value;          // From wb_inst of wb_stage.v
  logic [31:0]          wb_instruction;         // From wb_inst of wb_stage.v
  // End of automatics

  logic [2:0]           mmio_reg_number;
  logic [31:0]          mmio_reg_data_in;
  wire [31:0]           mmio_reg_data_out;
  logic                 mmio_reg_write;

  logic                 icache_flush;
  logic                 dcache_flush;
  wire                  dcache_flushing;
  
  logic [31:0]          core_state;
  logic [31:0]          core_state_next;

  logic                 PC_enable;
  logic [31:0]          PC_override;
  logic                 PC_override_valid;

  assign system_halt = !core_state[0];
  always @(posedge system_halt) begin
    $display("%c[1;32mPRIORITY MESSAGE:%c[0mCore %d halted!", 27, 27, core_no);
  end
  

  
  always_ff @(posedge clock) begin
    if (reset) begin
      core_state <= core_initial_state;
    end else begin
      core_state <= core_state_next;
    end
  end

  always_ff @(posedge clock) begin
    $display("&&&&&&&Core %d next_state %d", core_no, core_state);
  end
  
    
  always_comb begin
    core_state_next = core_state && !(ex_opcode == `HALT);
    PC_enable = core_state[0];
    PC_override = 0;
    PC_override_valid = 0;
    mmio_acknowledged = 0;
    mmio_data_out = 0;
    dcache_flush = 0;
    icache_flush = 0;
    mmio_reg_write = 0;
    mmio_reg_data_in = 0;
    mmio_reg_number = 0;
    if(mmio_address != 0) begin
      $display("Got MMIO request on core %d! addr %d, value %d, write %d",
               core_no, mmio_address, mmio_data_in, mmio_write);
      if(mmio_address[4:0] == 0) begin
        if (mmio_write) begin
          $display("Overriding core %d state to %d", core_no, mmio_data_in);	
          core_state_next = mmio_data_in;
        end
        mmio_data_out = core_state;
        mmio_acknowledged = 1;
      end

      if(mmio_address[4:0] == 1) begin
        if (mmio_write) begin
          $display("Overriding core %d PC to %d", core_no, mmio_data_in);
          PC_override = mmio_data_in;
          PC_override_valid = 1;
        end
        mmio_data_out = PC;
        mmio_acknowledged = 1;
      end
      if(mmio_address[4:0] == 2) begin
        if (mmio_write) begin
          if (mmio_data_in[0]) begin
            $display("Flushing icache on core %d", core_no);
            icache_flush = 0;
          end
          if (mmio_data_in[1]) begin
            $display("Flushing dcache on core %d", core_no);
            dcache_flush = 1;
          end
        end
        mmio_data_out = dcache_flushing << 1;
        mmio_acknowledged = 1;
      end // if (mmio_address[4:0] == 2)
      if (mmio_address[4:0] == 3) begin
        if (mmio_write && mmio_data_in == 32'hDEADC0DE) begin
          mmio_reset = 1;
        end
        mmio_acknowledged = 1;
      end
      if (mmio_address[4]) begin
        mmio_acknowledged = 1;
        mmio_reg_write = mmio_write;
        mmio_reg_data_in = mmio_data_in;
        mmio_data_out = mmio_reg_data_out;
        mmio_reg_number = mmio_address[2:0];
      end


    end // if (mmio_address != 0)
  end // always_comb

  
  pc pc_inst(/*AUTOINST*/
             // Outputs
             .PC                        (PC[31:0]),
             // Inputs
             .clock                     (clock),
             .reset                     (internal_reset),
             .fetch_PC_next             (fetch_PC_next[31:0]),
             .branch_PC_next            (branch_PC_next[31:0]),
             .branch_taken              (branch_taken),
             .PC_enable                 (PC_enable),
             .PC_override               (PC_override[31:0]),
             .PC_override_valid         (PC_override_valid));
  icache icache_inst(/*AUTOINST*/
                     // Outputs
                     .icache2core_data  (icache2core_data[31:0]),
                     .icache2core_valid (icache2core_valid),
                     .icache2mem_address(icache2mem_address[31:0]),
                     .icache2mem_start_txn(icache2mem_start_txn),
                     // Inputs
                     .clock             (clock),
                     .reset             (internal_reset),
                     .PC                (PC[31:0]),
                     .mem2icache_data   (mem2icache_data[31:0]),
                     .mem2icache_done   (mem2icache_done),
                     .icache_flush      (icache_flush));
  dcache dcache_inst(/*AUTOINST*/
                     // Outputs
                     .dcache2core_data  (dcache2core_data[31:0]),
                     .dcache2core_done  (dcache2core_done),
                     .dcache2mem_address(dcache2mem_address[31:0]),
                     .dcache2mem_data   (dcache2mem_data[31:0]),
                     .dcache2mem_bus_op (dcache2mem_bus_op),
                     .dcache2mem_start_txn(dcache2mem_start_txn),
                     .dcache_flushing (dcache_flushing),

                     // Inouts
                     .core2mem_HIT      (core2mem_HIT),
                     .core2mem_HITM     (core2mem_HITM),
                     .core2mem_HITM_data(core2mem_HITM_data[31:0]),
                     // Inputs
                     .clock             (clock),
                     .reset             (internal_reset),
                     .core2dcache_address(core2dcache_address[31:0]),
                     .core2dcache_data  (core2dcache_data[31:0]),
                     .core2dcache_write (core2dcache_write),
                     .core2dcache_start_txn(core2dcache_start_txn),
                     .mem2dcache_data   (mem2dcache_data[31:0]),
                     .mem2dcache_done   (mem2dcache_done),
                     .mem_bus_winner_address(mem_bus_winner_address[31:0]),
                     .mem_bus_winner_op (mem_bus_winner_op),
                     .bus2core_writeback_done(bus2core_writeback_done),
                     .dcache_flush (dcache_flush));
  mem_arbitrator mem_arbitrator_inst(/*AUTOINST*/
                                     // Outputs
                                     .mem2dcache_done   (mem2dcache_done),
                                     .mem2dcache_data   (mem2dcache_data[31:0]),
                                     .mem2icache_data   (mem2icache_data[31:0]),
                                     .mem2icache_done   (mem2icache_done),
                                     .core2mem_address  (core2mem_address[31:0]),
                                     .core2mem_data     (core2mem_data[31:0]),
                                     .core2mem_bus_op   (core2mem_bus_op),
                                     .core2mem_start_txn(core2mem_start_txn),
                                     // Inputs
                                     .dcache2mem_address(dcache2mem_address[31:0]),
                                     .dcache2mem_data   (dcache2mem_data[31:0]),
                                     .dcache2mem_bus_op (dcache2mem_bus_op),
                                     .dcache2mem_start_txn(dcache2mem_start_txn),
                                     .icache2mem_address(icache2mem_address[31:0]),
                                     .icache2mem_start_txn(icache2mem_start_txn),
                                     .mem2core_data     (mem2core_data[31:0]),
                                     .mem2core_done     (mem2core_done));
  if_stage if_inst(/*AUTOINST*/
                   // Outputs
                   .fetch_PC_next       (fetch_PC_next[31:0]),
                   .if_PC               (if_PC[31:0]),
                   .if_instruction      (if_instruction[31:0]),
                   .if_instruction_valid(if_instruction_valid),
                   // Inputs
                   .clock               (clock),
                   .reset               (internal_reset),
                   .squash              (squash),
                   .PC                  (PC[31:0]),
                   .PC_enable           (PC_enable),
                   .icache2core_data    (icache2core_data[31:0]),
                   .icache2core_valid   (icache2core_valid),
                   .id_stall            (id_stall));
  id_stage id_inst(/*AUTOINST*/
                   // Outputs
                   .id_stall            (id_stall),
                   .id_PC               (id_PC[31:0]),
                   .id_instruction      (id_instruction[31:0]),
                   .id_opcode           (id_opcode[3:0]),
                   .id_reg_a_idx        (id_reg_a_idx[2:0]),
                   .id_reg_a_value      (id_reg_a_value[31:0]),
                   .id_reg_a_check      (id_reg_a_check),
                   .id_reg_b_idx        (id_reg_b_idx[2:0]),
                   .id_reg_b_value      (id_reg_b_value[31:0]),
                   .id_reg_b_check      (id_reg_b_check),
                   .id_offset_field     (id_offset_field[31:0]),
                   .id_dest_reg         (id_dest_reg[2:0]),
                   .id_dest_reg_used    (id_dest_reg_used),
                   .id_valid            (id_valid),
                   .mmio_reg_data_out   (mmio_reg_data_out),
                   // Inputs
                   .clock               (clock),
                   .reset               (internal_reset),
                   .squash              (squash),
                   .if_PC               (if_PC[31:0]),
                   .if_instruction      (if_instruction[31:0]),
                   .if_instruction_valid(if_instruction_valid),
                   .ex_stall            (ex_stall),
                   .wb_dest_reg         (wb_dest_reg[2:0]),
                   .wb_dest_value       (wb_dest_value[31:0]),
                   .wb_dest_reg_used    (wb_dest_reg_used),
                   .mmio_reg_number     (mmio_reg_number),
                   .mmio_reg_data_in    (mmio_reg_data_in),
                   .mmio_reg_write      (mmio_reg_write));
  ex_stage ex_inst(/*AUTOINST*/
                   // Outputs
                   .ex_PC               (ex_PC[31:0]),
                   .ex_instruction      (ex_instruction[31:0]),
                   .ex_opcode           (ex_opcode[3:0]),
                   .ex_dest_reg         (ex_dest_reg[2:0]),
                   .ex_dest_reg_used    (ex_dest_reg_used),
                   .ex_dest_value       (ex_dest_value[31:0]),
                   .ex_address          (ex_address[31:0]),
                   .ex_branch_taken     (ex_branch_taken),
                   .ex_valid            (ex_valid),
                   .ex_stall            (ex_stall),
                   // Inputs
                   .clock               (clock),
                   .reset               (internal_reset),
                   .squash              (squash),
                   .id_PC               (id_PC[31:0]),
                   .id_instruction      (id_instruction[31:0]),
                   .id_opcode           (id_opcode[3:0]),
                   .id_reg_a_idx        (id_reg_a_idx[2:0]),
                   .id_reg_a_value      (id_reg_a_value[31:0]),
                   .id_reg_a_check      (id_reg_a_check),
                   .id_reg_b_idx        (id_reg_b_idx[2:0]),
                   .id_reg_b_value      (id_reg_b_value[31:0]),
                   .id_reg_b_check      (id_reg_b_check),
                   .id_offset_field     (id_offset_field[31:0]),
                   .id_dest_reg         (id_dest_reg[2:0]),
                   .id_dest_reg_used    (id_dest_reg_used),
                   .id_valid            (id_valid),
                   .mem_stall           (mem_stall),
                   .mem_dest_reg        (mem_dest_reg[2:0]),
                   .mem_dest_reg_used   (mem_dest_reg_used),
                   .mem_dest_value      (mem_dest_value[31:0]),
                   .mem_valid           (mem_valid),
                   .wb_dest_reg         (wb_dest_reg[2:0]),
                   .wb_dest_reg_used    (wb_dest_reg_used),
                   .wb_dest_value       (wb_dest_value[31:0]));
  mem_stage mem_inst(/*AUTOINST*/
                     // Outputs
                     .squash            (squash),
                     .mem_stall         (mem_stall),
                     .core2dcache_address(core2dcache_address[31:0]),
                     .core2dcache_data  (core2dcache_data[31:0]),
                     .core2dcache_write (core2dcache_write),
                     .core2dcache_start_txn(core2dcache_start_txn),
                     .mem_PC            (mem_PC[31:0]),
                     .mem_instruction   (mem_instruction[31:0]),
                     .mem_dest_reg      (mem_dest_reg[2:0]),
                     .mem_dest_reg_used (mem_dest_reg_used),
                     .mem_dest_value    (mem_dest_value[31:0]),
                     .mem_valid         (mem_valid),
                     .branch_taken      (branch_taken),
                     .branch_PC_next    (branch_PC_next[31:0]),
                     // Inputs
                     .clock             (clock),
                     .reset             (internal_reset),
                     .ex_PC             (ex_PC[31:0]),
                     .ex_instruction    (ex_instruction[31:0]),
                     .ex_opcode         (ex_opcode[3:0]),
                     .ex_dest_reg       (ex_dest_reg[2:0]),
                     .ex_dest_reg_used  (ex_dest_reg_used),
                     .ex_dest_value     (ex_dest_value[31:0]),
                     .ex_address        (ex_address[31:0]),
                     .ex_branch_taken   (ex_branch_taken),
                     .ex_valid          (ex_valid),
                     .dcache2core_data  (dcache2core_data[31:0]),
                     .dcache2core_done  (dcache2core_done),
                     .mem_bus_winner_address (mem_bus_winner_address),
                     .mem_bus_winner_op (mem_bus_winner_op),
                     .core_no (core_no));
  wb_stage wb_inst(/*AUTOINST*/
                   // Outputs
                   .wb_PC               (wb_PC[31:0]),
                   .wb_instruction      (wb_instruction[31:0]),
                   .wb_dest_reg         (wb_dest_reg[2:0]),
                   .wb_dest_reg_used    (wb_dest_reg_used),
                   .wb_dest_value       (wb_dest_value[31:0]),
                   // Inputs
                   .clock               (clock),
                   .reset               (internal_reset),
                   .mem_PC              (mem_PC[31:0]),
                   .mem_instruction     (mem_instruction[31:0]),
                   .mem_dest_reg        (mem_dest_reg[2:0]),
                   .mem_dest_reg_used   (mem_dest_reg_used),
                   .mem_dest_value      (mem_dest_value[31:0]),
                   .mem_valid           (mem_valid));

  
endmodule // pipeline
