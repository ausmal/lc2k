/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module ex_stage (
                 input               clock,
                 input               reset,
                 input               squash,
`ifdef DEBUG
                 input [31:0]        id_PC,
                 input [31:0]        id_instruction,
`endif
                
                 input [3:0]         id_opcode,
                 input [2:0]         id_reg_a_idx,
                 input [31:0]        id_reg_a_value,
                 input               id_reg_a_check,
                
                 input [2:0]         id_reg_b_idx,
                 input [31:0]        id_reg_b_value,
                 input               id_reg_b_check,

                 input [31:0]        id_offset_field,
                
                 input [2:0]         id_dest_reg,
                 input               id_dest_reg_used,

                 input               id_valid,

`ifdef DEBUG
                 output logic [31:0] ex_PC,
                 output logic [31:0] ex_instruction,
`endif
                 
                 output logic [3:0]  ex_opcode,

                 output logic [2:0]  ex_dest_reg,
                 output logic        ex_dest_reg_used,
                 output logic [31:0] ex_dest_value,

                 output logic [31:0] ex_address,
                 output logic        ex_branch_taken,

                 
                 output logic        ex_valid,

                 output logic        ex_stall,

                 /* This is the last place a stall can start. */
                 input logic         mem_stall,

                 input logic [2:0]   mem_dest_reg,
                 input logic         mem_dest_reg_used,
                 input logic [31:0]  mem_dest_value,
                 input logic         mem_valid,

                 input logic [2:0]   wb_dest_reg,
                 input logic         wb_dest_reg_used,
                 input logic [31:0]  wb_dest_value
                 
                 );
  
  logic [3:0]                 ex_opcode_next;

  logic [2:0]                 ex_dest_reg_next;
  logic                       ex_dest_reg_used_next;
  logic [31:0]                ex_dest_value_next;

  /* Used for either memory address or branch address */
  logic [31:0]                ex_address_next;
  logic                       ex_branch_taken_next;
  
	logic                       ex_valid_next;

  logic [31:0]                reg_a;
  logic [31:0]                reg_b;

  logic [31:0]                alu_op_a;
  logic [31:0]                alu_op_b;
  
  enum logic [2:0] 						{ALU_ADD, 
                               ALU_NAND, 
                               ALU_OR, 
                               ALU_AND, 
                               ALU_NOT,
                               ALU_SL,
                               ALU_SR,
                               ALU_SUB} alu_op;	
       
  logic [31:0]                alu_result;

  logic [31:0]                comp_op_a;
  logic [31:0]                comp_op_b;
  logic                       comp_result;

  /* Handle the hazards */
  always @(*) begin
    reg_a = id_reg_a_value;
    reg_b = id_reg_b_value;
    if (id_reg_a_check) begin
      if (id_reg_a_idx == ex_dest_reg && ex_dest_reg_used && ex_valid) begin
        reg_a = ex_dest_value;
      end else if (id_reg_a_idx == mem_dest_reg && mem_dest_reg_used && mem_valid) begin
        reg_a = mem_dest_value;
      end else if (id_reg_a_idx == wb_dest_reg && wb_dest_reg_used) begin
        reg_a = wb_dest_value;
      end
    end

    if (id_reg_b_check) begin
      if (id_reg_b_idx == ex_dest_reg && ex_dest_reg_used && ex_valid) begin
        reg_b = ex_dest_value;
      end else if (id_reg_b_idx == mem_dest_reg && mem_dest_reg_used && mem_valid) begin
        reg_b = mem_dest_value;
      end else if (id_reg_b_idx == wb_dest_reg && wb_dest_reg_used) begin
        reg_b = wb_dest_value;
      end
    end
  end // always @(*)


  
  /* The ALU */
  always @(*) begin
    case(alu_op)
      ALU_NAND: alu_result = ~(alu_op_a & alu_op_b);
      ALU_ADD:  alu_result = alu_op_a + alu_op_b;
      ALU_OR: alu_result = alu_op_a | alu_op_b;
      ALU_AND: alu_result = alu_op_a & alu_op_b;
      ALU_NOT: alu_result = ~alu_op_a;
      ALU_SL: alu_result = alu_op_a << alu_op_b[4:0];
      ALU_SR: alu_result = alu_op_b >> alu_op_b[4:0];
      ALU_SUB: alu_result = alu_op_a - alu_op_b;
    endcase
  end

  /* Equality Comparator */
  always @(*) begin
    comp_result = (comp_op_a == comp_op_b);
  end
  
  /* Muxes */
  always @(*) begin
    /* Defaults */
    ex_opcode_next = (id_valid) ? id_opcode : `NOOP;
    ex_dest_reg_next = id_dest_reg;
    ex_dest_reg_used_next = id_dest_reg_used;
    ex_dest_value_next = 0;
    ex_address_next = 0;
    ex_branch_taken_next = 0;
    ex_valid_next = id_valid;
    alu_op_a = reg_a;
    alu_op_b = reg_b;
    alu_op = ALU_ADD;
    comp_op_a = reg_a;
    comp_op_b = reg_b;
    if (id_opcode == `ADD) begin
      alu_op = ALU_ADD;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `NAND) begin
      alu_op = ALU_NAND;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `AND) begin
      alu_op = ALU_AND;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `OR) begin
      alu_op = ALU_OR;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `NOT) begin
      alu_op = ALU_NOT;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `SL) begin
      alu_op = ALU_SL;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `SR) begin
      alu_op = ALU_SR;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `SUB) begin
      alu_op = ALU_SUB;
      ex_dest_value_next = alu_result;
    end else if (id_opcode == `LW || id_opcode == `LWL) begin
      alu_op_b = id_offset_field;
      ex_address_next = alu_result;
    end else if (id_opcode == `SW || id_opcode == `SWC) begin
      alu_op_b = id_offset_field;
      ex_address_next = alu_result;
      ex_dest_value_next = reg_b;
    end else if (id_opcode == `BEQ) begin
      alu_op_a = id_PC + 1; //XXX: Pass this along from if stage.
      alu_op_b = id_offset_field;
      ex_address_next = alu_result;
      ex_branch_taken_next = comp_result;
    end else if (id_opcode == `JALR) begin
      comp_op_a = id_reg_a_idx;
      comp_op_b = id_reg_b_idx;
      ex_address_next = (comp_result) ? (id_PC + 1) : reg_a; //XXX: Same.
      ex_branch_taken_next = 1; /* Unconditional branch */
      ex_dest_value_next = (id_PC + 1); //XXX: Same.
    end

    ex_stall = mem_stall;
  end // always @(*)

  /* Flip-flops */
  always_ff @(posedge clock) begin
    if (reset || squash) begin
`ifdef DEBUG
      ex_PC <= 0;
      ex_instruction <= 0;
`endif
      
      ex_opcode <= 0;

      ex_dest_reg <= 0;
      ex_dest_reg_used <= 0;
      ex_dest_value <= 0;

      ex_address <= 0;
      ex_branch_taken <= 0;

      ex_valid <= 0;
    end else if (!mem_stall) begin // if (reset || squash)
`ifdef DEBUG
      ex_PC <= id_PC;
      ex_instruction <= id_instruction;
`endif
      
      ex_opcode <= ex_opcode_next;

      ex_dest_reg <= ex_dest_reg_next;
      ex_dest_reg_used <= ex_dest_reg_used_next;
      ex_dest_value <= ex_dest_value_next;

      ex_address <= ex_address_next;
      ex_branch_taken <= ex_branch_taken_next;

      ex_valid <= ex_valid_next;

    end // if (!mem_stall)
  end // always @
  
endmodule // ex_stage
