/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

typedef struct packed {
  logic [31:0] line;
  logic [31:0] tag;
  state_t state;

  } dcache_line_t;

module dcache(
              input               clock,
              input               reset,

              input [31:0]        core2dcache_address,
              input [31:0]        core2dcache_data,
              input               core2dcache_write,
              input               core2dcache_start_txn, 

              output logic [31:0] dcache2core_data,
              output logic        dcache2core_done,
              
              /*MEM side*/

              output logic [31:0] dcache2mem_address,
              output logic [31:0] dcache2mem_data,
              output              mem_bus_op_t dcache2mem_bus_op,
              output logic        dcache2mem_start_txn,

              input [31:0]        mem2dcache_data,
              input               mem2dcache_done,

              input [31:0]        mem_bus_winner_address,
              input               mem_bus_op_t mem_bus_winner_op,


              /* This might look a little strange if you're not super
               familiar with verilog. Verilog supports a type of wire
               called wor, which models an open-collector wired
               or. The value of the wire is basically the logical "or"
               of all the drivers of the wire. It can only be driven
               by assign statements. */
              
              inout wor           core2mem_HIT,
              inout wor           core2mem_HITM,
              inout wor [31:0]    core2mem_HITM_data,
              input               bus2core_writeback_done,

              input		dcache_flush,
              output logic dcache_flushing
              
              );

  logic                           our_HIT;
  logic                           our_HITM;
  logic [31:0]                    our_HITM_data;

  assign core2mem_HIT = our_HIT;
  assign core2mem_HITM = our_HITM;
  assign core2mem_HITM_data = our_HITM_data;
  
  dcache_line_t [31:0] lines;
  dcache_line_t [31:0] lines_next;

  logic                           writebacks_finished;
  
  always_ff @(posedge clock) begin
    if (reset || (writebacks_finished && !dcache_flush)) begin
      dcache_flushing <= 0;
    end else if (dcache_flush) begin
      dcache_flushing <= 1;
    end
  end


  always_comb begin
    dcache2mem_address = 0;
    dcache2mem_start_txn = 0;
    dcache2mem_bus_op = NONE;
    dcache2mem_data = 0;
    
    dcache2core_data = 32'hbeefbeef;
    dcache2core_done = 0;
    
    lines_next = lines;

    our_HIT = 0;
    our_HITM = 0;
    our_HITM_data = 0;

    writebacks_finished = 1;

    if (dcache_flushing) begin
      $display("DCACHE FLUSHING");
      for (int i = 0; i < 32; i++) begin
        if (lines[i].state == MODIFIED) begin
          writebacks_finished = 0;
          dcache2mem_address = lines[i].tag;
          dcache2mem_data = lines[i].line;
          dcache2mem_bus_op = BUS_WRITE_LINE;
          dcache2mem_start_txn = 1;

          lines_next[i].state = (mem2dcache_done) ? INVALID : MODIFIED;
        end
      end // for (int i = 0; i < 32; i++)
    end else begin // if (dcache_flushing)
      writebacks_finished = 0;
      /* Handle MESI */
      //XXX: Only works if memory latency is 1.
      if (!mem2dcache_done) begin
        //I think this is right. If we're the winner, it's done.
        our_HITM = (lines[mem_bus_winner_address[4:0]].state == MODIFIED &&
                    lines[mem_bus_winner_address[4:0]].tag == mem_bus_winner_address);
        our_HIT = ((lines[mem_bus_winner_address[4:0]].state == EXCLUSIVE ||
                    lines[mem_bus_winner_address[4:0]].state == SHARED) &&
                   lines[mem_bus_winner_address[4:0]].tag == mem_bus_winner_address);
        if (our_HITM) begin
          our_HITM_data = lines[mem_bus_winner_address[4:0]].line;
          lines_next[mem_bus_winner_address[4:0]].state = SHARED;
        end
        

        if (mem_bus_winner_op == BUS_INVALIDATE_LINE ||
            mem_bus_winner_op == BUS_READ_INVALIDATE_LINE &&
            lines_next[mem_bus_winner_address[4:0]].tag == mem_bus_winner_address) begin
          /* We've been told that we need to invalidate the line */
          $display("INVALIDATION!");
          lines_next[mem_bus_winner_address[4:0]].state = INVALID;
        end
        
        
        
      end
      
      
      if (core2dcache_start_txn) begin
        $display("DCACHE request for %d, LINE %d, ST %s, TAG %d", core2dcache_address, 
                 core2dcache_address[4:0], lines[core2dcache_address[4:0]].state,
                 lines[core2dcache_address[4:0]].tag);

        if (lines[core2dcache_address[4:0]].state != INVALID && 
            lines[core2dcache_address[4:0]].tag == core2dcache_address) begin
          $display("HIT on data at %d!", core2dcache_address);
          if (core2dcache_write) begin
            //See if we own the data before we overwrite it.
            if (lines[core2dcache_address[4:0]].state == MODIFIED ||
                lines[core2dcache_address[4:0]].state == EXCLUSIVE) begin
              //Hooray, we own the data.
              lines_next[core2dcache_address[4:0]].line = core2dcache_data;
              lines_next[core2dcache_address[4:0]].state = MODIFIED;
              dcache2core_done = 1;
              dcache2core_data = core2dcache_data;
            end else begin
              dcache2mem_address = core2dcache_address;
              dcache2mem_bus_op = BUS_INVALIDATE_LINE;
              dcache2mem_start_txn = 1;
              if (mem2dcache_done) begin
                lines_next[core2dcache_address[4:0]].line = core2dcache_data;
                lines_next[core2dcache_address[4:0]].state = MODIFIED;
                dcache2core_done = 1;
                dcache2core_data = core2dcache_data;
              end
            end
          end else begin
            //Doing a read
            dcache2core_data = lines[core2dcache_address[4:0]].line;
            dcache2core_done = 1;
          end
        end else begin // if (lines[core2dcache_address[4:0]].state != INVALID &&...
          // We don't have this line.
          $display("MISS on data at %d!", core2dcache_address);
          if (core2dcache_address[30]) begin
            //This is a MMIO address, so not cachable
            dcache2mem_bus_op = (core2dcache_write) ?
                                BUS_WRITE_LINE : BUS_READ_LINE;
            dcache2mem_address = core2dcache_address;
            dcache2mem_data = core2dcache_data;
            dcache2core_data = mem2dcache_data;
            dcache2core_done = mem2dcache_done;
            dcache2mem_start_txn = 1;
          end else begin
            //Let's see if there's modified data there now.
            if (lines[core2dcache_address[4:0]].state == MODIFIED) begin
              $display("EVICTING A LINE!");
              //Write back the data
              dcache2mem_address = lines[core2dcache_address[4:0]].tag;
              dcache2mem_data = lines[core2dcache_address[4:0]].line;
              dcache2mem_bus_op = BUS_WRITE_LINE;
              dcache2mem_start_txn = 1;
              if (mem2dcache_done) begin
                //And mark the line as invalid so we can replace it next cycle.
                lines_next[core2dcache_address[4:0]].state = INVALID;
              end
            end else begin
              //Need to send an exclusive read if writing.
              dcache2mem_bus_op = (core2dcache_write) ?
                                  BUS_READ_INVALIDATE_LINE : BUS_READ_LINE;
              dcache2mem_address = core2dcache_address;
              dcache2mem_start_txn = 1;
              if (mem2dcache_done) begin
                lines_next[core2dcache_address[4:0]].state = (core2dcache_write) ?
                                                             MODIFIED : (core2mem_HIT) ? 
                                                             SHARED : EXCLUSIVE;
                assert (!core2mem_HITM);
                lines_next[core2dcache_address[4:0]].tag = core2dcache_address;
                lines_next[core2dcache_address[4:0]].line = (core2dcache_write) ?
                                                            core2dcache_data : mem2dcache_data;

                //Send the data out to the core.
                dcache2core_data = (core2dcache_write) ? core2dcache_data : mem2dcache_data;
                dcache2core_done = 1;
              end // if (mem2dcache_done)
            end // else: !if(lines[core2dcache_address[4:0]].state == MODIFIED)
          end // else: !if(core2dcache_address[30])
        end // else: !if(line.valid && line.tag = core2dcache_address)
      end // if (core2dcache_start_txn)
    end // else: !if(dcache_flusing)
  end // always @ (posedge clock)

  always_ff @(posedge clock) begin
    if (reset || (dcache_flushing && writebacks_finished)) begin
      lines <= 0;
    end else begin
      lines <= lines_next;
    end
  end
  
endmodule // dcache
