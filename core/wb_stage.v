/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module wb_stage(
                input               clock,
                input               reset,

`ifdef DEBUG
                input [31:0]        mem_PC,
                input [31:0]        mem_instruction,
`endif

                input [2:0]         mem_dest_reg,
                input               mem_dest_reg_used,
                input [31:0]        mem_dest_value,

                input               mem_valid,

`ifdef DEBUG
                output logic [31:0] wb_PC,
                output logic [31:0] wb_instruction,
`endif
                                
                output logic [2:0]  wb_dest_reg,
                output logic        wb_dest_reg_used,
                output logic [31:0] wb_dest_value
                );

  assign wb_PC = mem_PC;
  assign wb_instruction = mem_instruction;
  assign wb_dest_reg_used = (mem_valid && mem_dest_reg_used);
  assign wb_dest_value = mem_dest_value;
  assign wb_dest_reg = mem_dest_reg;
  
endmodule // wb_stage

