/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module if_stage(
                input               clock,
                input               reset,
                input               squash,

                input [31:0]        PC,
                input               PC_enable,

                input [31:0]        icache2core_data,
                input               icache2core_valid,
                
                output logic [31:0] fetch_PC_next,

`ifdef DEBUG
                output logic [31:0] if_PC,
`endif
                
                output logic [31:0] if_instruction,
                output logic        if_instruction_valid,

                /* Combinational signal from id_stage to tell us to hold output */
                input               id_stall 
                );

  logic [31:0]                      if_instruction_next;
  logic                             if_instruction_valid_next;

  /* Next PC is PC or PC + 1 depending on whether or not we stall */
  assign fetch_PC_next = (id_stall || !icache2core_valid) ? PC : PC + 1;

  assign if_instruction_valid_next = icache2core_valid && PC_enable;
  assign if_instruction_next = (icache2core_valid) ? icache2core_data : 32'hfeedbeef;
  
  /* Flip flop outputs */
  always_ff @(posedge clock) begin
    if (reset || squash) begin
      if_instruction <= 32'hdeadbeef;
      if_instruction_valid <= 0;
`ifdef DEBUG
      if_PC <= 0;
`endif
    end else if (!id_stall) begin
      if_instruction <= if_instruction_next;
      if_instruction_valid <= if_instruction_valid_next;
`ifdef DEBUG
      if_PC <= (if_instruction_valid_next) ? PC : 32'hdeadbeef;
`endif
    end
  end // always @
      
endmodule // if_stage	

                
