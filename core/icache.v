/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef struct packed {
  logic [31:0] line;
  logic [31:0] tag;
  logic        valid;
  } icache_line_t;

module icache(
              input               clock,
              input               reset,

              input [31:0]        PC,

              output logic [31:0] icache2core_data,
              output logic        icache2core_valid,

              output logic [31:0] icache2mem_address,
              output logic        icache2mem_start_txn,

              input [31:0]        mem2icache_data,
              input               mem2icache_done,

              input 							icache_flush
              
              );
  
  icache_line_t [31:0] lines;
  icache_line_t [31:0] lines_next;

  always @(*) begin
    icache2mem_address = 0;
    icache2mem_start_txn = 0;
    icache2core_data = 32'hbeefbeef;
    icache2core_valid = 0;
    lines_next = lines;


    if (lines[PC[4:0]].valid && lines[PC[4:0]].tag == PC) begin
      $display("HIT on instruction at %d!", PC);
      icache2core_data = lines[PC[4:0]].line;
      icache2core_valid = 1;
    end else begin
      $display("MISS on instruction at %d!", PC);
      icache2mem_address = PC;
      icache2mem_start_txn = 1;
      if (mem2icache_done) begin
        lines_next[PC[4:0]].line = mem2icache_data;
        lines_next[PC[4:0]].tag = PC;
        lines_next[PC[4:0]].valid = 1;
        icache2core_data = mem2icache_data;
        icache2core_valid = 1;
      end
    end // else: !if(line.valid && line.tag = PC)
  end // always @ (posedge clock)
    

  
  always_ff @(posedge clock) begin
    if (reset || icache_flush) begin
      lines <= 0;
    end else begin
      lines <= lines_next;
    end
  end
  
  
  
endmodule // icache
