/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module mem_stage(
                 input               clock,
                 input               reset,
                 output logic        squash,

`ifdef DEBUG
                 input [31:0]        ex_PC,
                 input [31:0]        ex_instruction,
`endif           

                 input [3:0]         ex_opcode,

                 input [2:0]         ex_dest_reg,
                 input               ex_dest_reg_used,
                 input [31:0]        ex_dest_value,

                 input [31:0]        ex_address,
                 input               ex_branch_taken,

                 input               ex_valid,

                 output logic        mem_stall,

                 output logic [31:0] core2dcache_address,
                 output logic [31:0] core2dcache_data,
                 output logic        core2dcache_write,
                 output logic        core2dcache_start_txn,

                 input [31:0]        dcache2core_data,
                 input               dcache2core_done,

`ifdef DEBUG
                 output logic [31:0] mem_PC,
                 output logic [31:0] mem_instruction,
`endif
                                  
                 output logic [2:0]  mem_dest_reg,
                 output logic        mem_dest_reg_used,
                 output logic [31:0] mem_dest_value,

                 output logic        mem_valid,
                 
                 output logic        branch_taken, 
                 output logic [31:0] branch_PC_next,

                 input [31:0]        mem_bus_winner_address,
                 input mem_bus_op_t  mem_bus_winner_op,

                 input [2:0]				 core_no

                 );

  logic [2:0]                 mem_dest_reg_next;
  logic                       mem_dest_reg_used_next;
  logic [31:0]                mem_dest_value_next;
  logic                       mem_valid_next;

  logic [31:0]                load_lock_address;
  logic [31:0]                load_lock_address_next;
  logic                       load_lock_lock;
  logic                       load_lock_lock_next;

  always_ff @(posedge clock) begin
    if (reset) begin
      load_lock_lock <= 0;
      load_lock_address <= 0;
    end else begin
      load_lock_lock <= load_lock_lock_next;
      load_lock_address <= load_lock_address_next;
    end
  end
  
  
  /* Interact with the memory system */ 
  always @(*) begin
    core2dcache_address = 0;
    core2dcache_data = 0;
    core2dcache_write = 0;
    core2dcache_start_txn = 0;
    mem_stall = 0;
    mem_dest_reg_next = ex_dest_reg;
    mem_dest_reg_used_next = ex_dest_reg_used;
    mem_dest_value_next = ex_dest_value;
    load_lock_address_next = load_lock_address;
    load_lock_lock_next = load_lock_lock;
    if (ex_valid) begin
      if (ex_opcode == `LW || ex_opcode == `LWL) begin
        core2dcache_address = ex_address;
        core2dcache_start_txn = 1;
        mem_stall = !dcache2core_done;
        mem_dest_value_next = dcache2core_data;
        if (ex_opcode == `LWL) begin
          load_lock_address_next = ex_address;
          load_lock_lock_next = 1;
        end
      end else if (ex_opcode == `SW) begin
        core2dcache_address = ex_address;
        core2dcache_data = ex_dest_value;
        core2dcache_write = 1;
        core2dcache_start_txn = 1;
        mem_stall = !dcache2core_done;
        if (load_lock_address == ex_address) begin
          load_lock_lock_next = 0;
        end
      end else if (ex_opcode == `SWC) begin
        $display("STORE CONDITIONAL SUCCEEDED ON CORE %d!", core_no);
        if (load_lock_address == ex_address && load_lock_lock) begin
          core2dcache_address = ex_address;
          core2dcache_data = ex_dest_value;
          core2dcache_write = 1;
          core2dcache_start_txn = 1;
          mem_stall = !dcache2core_done;
          mem_dest_value_next = 1;
          load_lock_lock_next = 0;
        end else begin
          $display("STORE CONDITIONAL FAILED ON CORE %d!", core_no);
          mem_dest_value_next = 0;
        end
      end
    end // if (ex_valid)
    mem_valid_next = ex_valid && !mem_stall;

    if (mem_bus_winner_address == load_lock_address &&
        (mem_bus_winner_op == BUS_INVALIDATE_LINE ||
         mem_bus_winner_op == BUS_READ_INVALIDATE_LINE || 
         mem_bus_winner_op == BUS_WRITE_LINE)) begin
      load_lock_lock_next = 0;
    end
    
  end // always @(*)
  
  
  /* Handle the branches */
  always @(*) begin 
    branch_taken = ex_branch_taken;
    branch_PC_next = ex_address;
    squash = ex_branch_taken || (ex_opcode == `HALT);
  end
  
  /* Flip-flops */
  always_ff @(posedge clock) begin
    if(reset) begin
`ifdef DEBUG
      mem_PC <= 0;
      mem_instruction <= 0;
`endif
      mem_dest_reg <= 0;
      mem_dest_reg_used <= 0;
      mem_dest_value <= 0;
      mem_valid <= 0;
    end else begin
`ifdef DEBUG
      mem_PC <= ex_PC;
      mem_instruction <= ex_instruction;
`endif
      mem_dest_reg <= mem_dest_reg_next;
      mem_dest_reg_used <= mem_dest_reg_used_next;
      mem_dest_value <= mem_dest_value_next;
      mem_valid <= mem_valid_next;
    end // else: !if(reset)
    
  end // always @
  
  
endmodule // mem_stage

