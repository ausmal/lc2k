/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module mem_arbitrator(
                      input [31:0]        dcache2mem_address,
                      input [31:0]        dcache2mem_data,
                      input               mem_bus_op_t dcache2mem_bus_op,
                      input               dcache2mem_start_txn,

                      output logic        mem2dcache_done,
                      output logic [31:0] mem2dcache_data,

                      input [31:0]        icache2mem_address,
                      input               icache2mem_start_txn,
                      
                      output logic [31:0] mem2icache_data,
                      output logic        mem2icache_done,
                      
                      output logic [31:0] core2mem_address,
                      output logic [31:0] core2mem_data,
                      output              mem_bus_op_t core2mem_bus_op,
                      output logic        core2mem_start_txn,

                      input [31:0]        mem2core_data,
                      input               mem2core_done

                                            
                      );

  always @(*) begin
    mem2dcache_done = 0;
    mem2dcache_data = 32'hdeadbeef;
    mem2icache_done = 0;
    mem2icache_data = 32'hbaaa_aaad;

    
    if (dcache2mem_start_txn) begin
      core2mem_address = dcache2mem_address;
      core2mem_data = dcache2mem_data;
      core2mem_bus_op = dcache2mem_bus_op;
      core2mem_start_txn = 1;
      mem2dcache_done = mem2core_done;
      mem2dcache_data = mem2core_data;
    end else begin
      core2mem_address = icache2mem_address;
      core2mem_data = 0;
      core2mem_bus_op = BUS_READ_LINE;
      core2mem_start_txn = icache2mem_start_txn;
      mem2icache_done = mem2core_done;
      mem2icache_data = mem2core_data;
    end // else: !if(dcache2mem_start_txn)
    
  end 
  
  
endmodule // mem_arbitrator
