/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "sys_defs.vh"

module id_stage(
                input               clock,
                input               reset,
                input               squash,

`ifdef DEBUG
                input [31:0]        if_PC,
`endif
                                
                input [31:0]        if_instruction,
                input               if_instruction_valid,
                
                output logic        id_stall,

`ifdef DEBUG
                output logic [31:0] id_PC,
                output logic [31:0] id_instruction,
`endif
                
                output logic [3:0]  id_opcode,
                output logic [2:0]  id_reg_a_idx,
                output logic [31:0] id_reg_a_value,
                output logic        id_reg_a_check,
                
                output logic [2:0]  id_reg_b_idx,
                output logic [31:0] id_reg_b_value,
                output logic        id_reg_b_check,

                output logic [31:0] id_offset_field,
                
                output logic [2:0]  id_dest_reg,
                output logic        id_dest_reg_used,

                output logic        id_valid,
                
                input               ex_stall, 

                input [2:0]         wb_dest_reg,
                input [31:0]        wb_dest_value,
                input               wb_dest_reg_used,

                input [2:0]         mmio_reg_number,
                input [31:0]        mmio_reg_data_in,
                output logic [31:0] mmio_reg_data_out,
                input               mmio_reg_write
                );

  /* Using unpacked arrays to work around a bug in icarus verilog */
  logic [31:0]                      registers [7:0];
  logic [31:0]                      registers_next[7:0];

  int                               i;
  int                               j;
  always_ff @(posedge clock) begin
    if (reset) begin
      for (i = 0; i < 8; i++) begin
        registers[i] <= 0;
      end
    end else begin
      for (i = 0; i < 8; i++) begin
        registers[i] <= registers_next[i];
      end
    end
  end

  assign mmio_reg_data_out = registers[mmio_reg_number];
  
  always @(*) begin
    for (j = 0; j < 8; j++) begin
      registers_next[j] = registers[j];
    end
    
    if (wb_dest_reg_used) begin
      registers_next[wb_dest_reg] = wb_dest_value;
    end

    if (mmio_reg_write) begin
      registers_next[mmio_reg_number] = mmio_reg_data_in;
    end
  end // always @ begin
  
  wire [2:0]                        reg_a = if_instruction[21:19];
  wire [2:0]                        reg_b = if_instruction[18:16];
  wire [2:0]                        dest_reg = if_instruction[2:0];
  wire [3:0]                        opcode = if_instruction[25:22];

  logic [3:0]                       id_opcode_next;
  
  logic [2:0]                       id_reg_a_idx_next;
  logic [31:0]                      id_reg_a_value_next;
  logic                             id_reg_a_check_next;
  logic [2:0]                       id_reg_b_idx_next;
  logic [31:0]                      id_reg_b_value_next;
  logic                             id_reg_b_check_next;

  logic [31:0]                      id_offset_field_next;
  
  logic [2:0]                       id_dest_reg_next;
  logic                             id_dest_reg_used_next;

  logic                             id_valid_next;
  
  always @(*) begin
    id_opcode_next = `NOOP;
    
    id_reg_a_idx_next = 0;
    id_reg_a_value_next = 0;
    id_reg_a_check_next = 0;
    id_reg_b_idx_next = 0;
    id_reg_b_value_next = 0;
    id_reg_b_check_next = 0;
    
    id_dest_reg_next = 0;
    id_dest_reg_used_next = 0;
    id_valid_next = 0;

    id_offset_field_next = 0;

    id_stall = 0;

    /*  Do the decoding */
    if (if_instruction_valid) begin
      id_valid_next = 1;

      id_opcode_next = opcode;

      id_reg_a_idx_next = reg_a;
      id_reg_a_value_next = registers_next[reg_a];
      id_reg_a_check_next = (opcode != `HALT && opcode != `NOOP);

      id_reg_b_idx_next = reg_b;
      id_reg_b_value_next = registers_next[reg_b];
      id_reg_b_check_next = (opcode != `HALT && opcode != `NOOP && 
                             opcode != `LW && opcode != `LWL && opcode != `JALR);

      
      id_offset_field_next = {{16{if_instruction[15]}},if_instruction[15:0]};

      
      id_dest_reg_next = (opcode == `LW || opcode == `LWL || 
                          opcode == `JALR || opcode == `SWC) ?
                         reg_b :	dest_reg;

      id_dest_reg_used_next = !(opcode ==  `SW || opcode == `BEQ || 
                                opcode == `HALT || opcode == `NOOP);

      if ((id_reg_a_check_next && reg_a == id_dest_reg && 
           (id_opcode == `LW || id_opcode == `LWL || id_opcode == `SWC) && id_valid) ||
          (id_reg_b_check_next && reg_b == id_dest_reg && 
           (id_opcode == `LW || id_opcode == `LWL || id_opcode == `SWC) && id_valid)) begin
        id_stall = 1;
        id_valid_next = 0;
        id_opcode_next = `NOOP;
      end

      if (ex_stall) begin
        id_stall = 1;
      end
    end // if (if_instruction_valid)
  end // always @(*)
  
  
  always_ff @(posedge clock) begin
    if (reset || squash) begin
      id_opcode <= `NOOP;
      
      id_reg_a_idx <= 0;
      id_reg_a_value <= 0;
      id_reg_a_check <= 0;
      id_reg_b_idx <= 0;
      id_reg_b_value <= 0;
      id_reg_b_check <= 0;

      id_dest_reg <= 0;
      id_dest_reg_used <= 0;
      id_valid <= 0;

      id_offset_field <= 0;
      
`ifdef DEBUG
      id_PC <= 32'hbaadbaad;
      id_instruction <= 32'hdeadbeef;
`endif
    end else if (!ex_stall) begin
      id_opcode <= id_opcode_next;

      id_reg_a_idx <= id_reg_a_idx_next;
      id_reg_a_value <= id_reg_a_value_next;
      id_reg_a_check <= id_reg_a_check_next;
      id_reg_b_idx <= id_reg_b_idx_next;
      id_reg_b_value <= id_reg_b_value_next;
      id_reg_b_check <= id_reg_b_check_next;

      id_dest_reg <= id_dest_reg_next;
      id_dest_reg_used <= id_dest_reg_used_next;
      id_valid <= id_valid_next;

      id_offset_field <= id_offset_field_next;
`ifdef DEBUG
      id_PC <= if_PC;
      id_instruction <= if_instruction;
`endif
    end // else: !if(reset || squash)
  end // always @
  
endmodule // id_stage

