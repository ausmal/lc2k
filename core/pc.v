module pc(
          input               clock,
          input               reset,

          output logic [31:0] PC,

          input [31:0]        fetch_PC_next,
          input [31:0]        branch_PC_next,
          input               branch_taken,

          input               PC_enable,
          input [31:0]        PC_override,
          input               PC_override_valid);

  always_ff @(posedge clock) begin
    if (reset) begin
      PC <= 0;
    end else if (PC_override_valid) begin
      PC <= PC_override;
    end else if (branch_taken && PC_enable) begin
      PC <= branch_PC_next;
    end else if (PC_enable) begin
      PC <= fetch_PC_next;
    end
  end
  
endmodule // pc
