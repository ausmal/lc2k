module console (
                input               clock,
                input               reset,

                /* Processor side */
                input [31:0]        console_address,
                input [31:0]        console_data_in,
                output logic [31:0] console_data_out,
                input               console_write,
                output logic        console_done,


                /* External world */
                input [7:0]         char_in,
                input               char_in_valid,
                output logic        char_in_acknowledged,


                output logic [7:0]  char_out,
                output logic        char_out_valid,
                input               char_out_acknowledged
                );


  logic [1023:0][7:0] tx_buffer;
  logic [9:0]         tx_buffer_head;
  logic [9:0]         tx_buffer_tail;
  logic [1023:0][7:0] tx_buffer_next;
  logic [9:0]         tx_buffer_head_next;
  logic [9:0]         tx_buffer_tail_next;

  logic [1023:0][7:0] rx_buffer;
  logic [9:0]         rx_buffer_head;
  logic [9:0]         rx_buffer_tail;
  logic [1023:0][7:0] rx_buffer_next;
  logic [9:0]         rx_buffer_head_next;
  logic [9:0]         rx_buffer_tail_next;


  always_comb begin
    char_in_acknowledged = 0;
    char_out = 0;
    char_out_valid = 0;
    console_data_out = 0;
    console_done = 0;

    tx_buffer_next = tx_buffer;
    tx_buffer_head_next = tx_buffer_head;
    tx_buffer_tail_next = tx_buffer_tail;
    rx_buffer_next = rx_buffer;
    rx_buffer_head_next = rx_buffer_head;
    rx_buffer_tail_next = rx_buffer_tail;
    
    if (char_in_valid) begin
      $display("Receiving character %c", char_in);
      rx_buffer_next[rx_buffer_tail] = char_in;
      rx_buffer_tail_next = rx_buffer_tail + 1;
      char_in_acknowledged = 1;
    end

    if (tx_buffer_head != tx_buffer_tail) begin
      $display("Sending character %c", tx_buffer[tx_buffer_head]);
      char_out = tx_buffer[tx_buffer_head];
      char_out_valid = 1;
      if (char_out_acknowledged) begin
        tx_buffer_head_next = tx_buffer_head+1;
      end

    end

    /* Core wants us to do something */
    if (console_address != 0) begin
      if (console_write) begin
        $display("Writing character %c", console_data_in[7:0]);
        tx_buffer_next[tx_buffer_tail] = console_data_in[7:0];
        tx_buffer_tail_next = tx_buffer_tail + 1;
        console_done = 1;
      end else begin
        if (console_address[0] == 0) begin
          /* Will read synchronously. */
          if (rx_buffer_tail == rx_buffer_head) begin
            /* Nothing to read. :( */
            
          end else begin
            console_data_out = {24'h0, rx_buffer[rx_buffer_head]};
            rx_buffer_head_next = rx_buffer_head + 1;
            console_done = 1;
          end
        end else begin
          /* Will read asynchronously. */
          if (rx_buffer_tail == rx_buffer_head) begin
            /* Nothing to read. :( */
            console_data_out = 32'hff;
            console_done = 1;
          end else begin
            console_data_out = {24'h0, rx_buffer[rx_buffer_head]};
            rx_buffer_head_next = rx_buffer_head + 1;
            console_done = 1;
          end
        end // else: !if(address[0] = 0)
      end
    end
    


  end

  always_ff @(posedge clock) begin
    if (reset) begin
      tx_buffer <= 0;
      tx_buffer_head <= 0;
      tx_buffer_tail <= 0;
      rx_buffer <= 0;
      rx_buffer_head <= 0;
      rx_buffer_tail <= 0;
    end else begin
      tx_buffer <= tx_buffer_next;
      tx_buffer_head <= tx_buffer_head_next;
      tx_buffer_tail <= tx_buffer_tail_next;
      rx_buffer <= rx_buffer_next;
      rx_buffer_head <= rx_buffer_head_next;
      rx_buffer_tail <= rx_buffer_tail_next;
    end
  end
  
endmodule
