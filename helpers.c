#include "vpi_user.h"
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

#define TB(x) "single_testbench."x

pthread_t child;

sem_t reading_sem;
volatile int last_input;
volatile int reading_done;
volatile int kill_child;
pid_t pid;

FILE *fil_o, *fil_i;

void * child_thread(void * args) {
  vpi_printf("Child started!");
  while(!kill_child) {
    sem_wait(&reading_sem);
    vpi_printf("+WAIT\r\n");
    last_input = fgetc(fil_i);
    vpi_printf("+DONE\r\n");
    reading_done = 1;
  }
  vpi_printf("Child finished!");
  return NULL;
}

void init_input() {
  vpi_printf("Initializing input");
  sem_init(&reading_sem, 0, 1);
  last_input = 0;
  reading_done = 0;
  kill_child = 0;
  
  fil_i = fopen("test_i", "r+");
  fil_o = fopen("test_o", "w+");
  setvbuf(fil_i, NULL, _IONBF, 0);
  setvbuf(fil_o, NULL, _IONBF, 0);
  pthread_create(&child, NULL, child_thread, NULL);

}

void get_input() {
  vpiHandle obj = vpi_handle_by_name(TB("char_from_c"), NULL);

  s_vpi_value v = {vpiIntVal, 0};
  if (reading_done) {
    v.value.integer = last_input;
    reading_done = 0;
    last_input = 0;
    sem_post(&reading_sem);
  } else {
    v.value.integer = -2;
  }
  vpi_put_value(obj, &v, NULL, vpiNoDelay);

}

void get_output() {
  vpiHandle obj = vpi_handle_by_name(TB("char_to_c"), NULL);
  s_vpi_value v = {vpiIntVal, 0};
  vpi_get_value(obj, &v);
  
  fputc(v.value.integer, fil_o);

}

void finish_input() {
  pthread_cancel(child);
  fclose(fil_i);
  fclose(fil_o);
}


void registerFinishSystfs() {
  s_vpi_systf_data task_data_s;
  p_vpi_systf_data task_data_p = &task_data_s;
  task_data_p->type = vpiSysTask;
  task_data_p->tfname = "$finish_input";
  task_data_p->calltf=finish_input;
  task_data_p->compiletf=0;

  vpi_register_systf(task_data_p);
}


void registerInitSystfs() {
  s_vpi_systf_data task_data_s;
  p_vpi_systf_data task_data_p = &task_data_s;
  task_data_p->type = vpiSysTask;
  task_data_p->tfname = "$init_input";
  task_data_p->calltf=init_input;
  task_data_p->compiletf=0;

  vpi_register_systf(task_data_p);
}


void registerGetinputSystfs() {
  s_vpi_systf_data task_data_s;
  p_vpi_systf_data task_data_p = &task_data_s;
  task_data_p->type = vpiSysTask;
  task_data_p->tfname = "$get_input";
  task_data_p->calltf=get_input;
  task_data_p->compiletf=0;

  vpi_register_systf(task_data_p);
}

void registerGetouputSystfs() {
  s_vpi_systf_data task_data_s;
  p_vpi_systf_data task_data_p = &task_data_s;
  task_data_p->type = vpiSysTask;
  task_data_p->tfname = "$get_output";
  task_data_p->calltf=get_output;
  task_data_p->compiletf=0;

  vpi_register_systf(task_data_p);
}

void (*vlog_startup_routines[ ])() = {
  registerGetinputSystfs,
  registerGetouputSystfs,
  registerInitSystfs,
  registerFinishSystfs,
  0
};
