/*  This file is part of LC2K-SV
    Copyright (C) 2014	Austin Maliszewski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
`include "sys_defs.vh"

module dual_testbench();

  logic clock;
  logic reset;

  int   clocks;

  always @(posedge clock) begin
    if (!reset) begin
      clocks++;
    end
  end
  

always begin
  #5
  clock = ~clock;
end

  logic [3:0][31:0]          core2mem_address;       // From pipe_inst of pipeline.v
  mem_bus_op_t [3:0]        core2mem_bus_op;        // From pipe_inst of pipeline.v
  logic [3:0][31:0]          core2mem_data;          // From pipe_inst of pipeline.v
  logic [3:0]                core2mem_start_txn;     // From pipe_inst of pipeline.v
  logic [3:0]                mem2core_done;          // From memory_bus_inst of memory_bus.v

  logic [3:0]                mmio_acknowledged;      // From memory_bus_inst of memory_bus.v, ...
  logic [3:0] [31:0]         mmio_address;           // From memory_bus_inst of memory_bus.v
  logic [3:0] [31:0]         mmio_data_in;           // From memory_bus_inst of memory_bus.v
  logic [3:0] [31:0]         mmio_data_out;          // From pipe_inst of pipeline.v
  logic [3:0]                mmio_write;             // From memory_bus_inst of memory_bus.v
  
  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  logic                 bus2core_writeback_done;// From memory_bus_inst of memory_bus.v
  logic [31:0]          bus2mem_address;        // From memory_bus_inst of memory_bus.v
  logic [31:0]          bus2mem_data;           // From memory_bus_inst of memory_bus.v
  logic                 bus2mem_start_txn;      // From memory_bus_inst of memory_bus.v
  logic                 bus2mem_write;          // From memory_bus_inst of memory_bus.v
  wor                   core2mem_HIT;           // To/From memory_bus_inst of memory_bus.v, ...
  wor                   core2mem_HITM;          // To/From memory_bus_inst of memory_bus.v, ...
  wor [31:0]            core2mem_HITM_data;     // To/From memory_bus_inst of memory_bus.v, ...
  logic [31:0]          mem2bus_data;           // From mem_inst of memory.v
  logic                 mem2bus_done;           // From mem_inst of memory.v
  logic [31:0]          mem2core_data;          // From memory_bus_inst of memory_bus.v
  logic [31:0]          mem_bus_winner_address; // From memory_bus_inst of memory_bus.v
  mem_bus_op_t          mem_bus_winner_op;      // From memory_bus_inst of memory_bus.v
  
  logic [3:0]                system_halt;            // From pipe_inst of pipeline.v
  // End of automatics

  logic                 mem_reset;

  assign core2mem_start_txn[3:2] = 0;
  assign system_halt[3:2] = 0;
  
  memory mem_inst(/*AUTOINST*/
                  // Outputs
                  .mem2bus_data         (mem2bus_data[31:0]),
                  .mem2bus_done         (mem2bus_done),
                  // Inputs
                  .clock                (clock),
                  .reset                (mem_reset),
                  .bus2mem_address      (bus2mem_address[31:0]),
                  .bus2mem_data         (bus2mem_data[31:0]),
                  .bus2mem_write        (bus2mem_write),
                  .bus2mem_start_txn    (bus2mem_start_txn));
  memory_bus memory_bus_inst(/*AUTOINST*/
                             // Outputs
                             .mem2core_data     (mem2core_data[31:0]),
                             .mem2core_done     (mem2core_done[3:0]),
                             .bus2core_writeback_done(bus2core_writeback_done),
                             .mem_bus_winner_address(mem_bus_winner_address[31:0]),
                             .mem_bus_winner_op (mem_bus_winner_op),
                             .bus2mem_address   (bus2mem_address[31:0]),
                             .bus2mem_data      (bus2mem_data[31:0]),
                             .bus2mem_write     (bus2mem_write),
                             .bus2mem_start_txn (bus2mem_start_txn),
                             .mmio_address      (mmio_address/*[3:0][31:0]*/),
                             .mmio_data_in      (mmio_data_in/*[3:0][31:0]*/),
                             .mmio_write        (mmio_write[3:0]),
                             .mmio_acknowledged (mmio_acknowledged/*[3:0][31:0]*/),
                             // Inouts
                             .core2mem_HIT      (core2mem_HIT),
                             .core2mem_HITM     (core2mem_HITM),
                             .core2mem_HITM_data(core2mem_HITM_data[31:0]),
                             // Inputs
                             .clock	(clock),
                             .reset (reset),
                             .core2mem_address  (core2mem_address/*[3:0][31:0]*/),
                             .core2mem_data     (core2mem_data/*[3:0][31:0]*/),
                             .core2mem_bus_op   (core2mem_bus_op[3:0]),
                             .core2mem_start_txn(core2mem_start_txn[3:0]),
                             .mem2bus_data      (mem2bus_data[31:0]),
                             .mem2bus_done      (mem2bus_done),
                             .mmio_data_out     (mmio_data_out/*[3:0][31:0]*/));
  pipeline pipe_inst0(/*AUTOINST*/
                     // Outputs
                     .core2mem_address  (core2mem_address[0]),
                     .core2mem_data     (core2mem_data[0]),
                     .core2mem_bus_op   (core2mem_bus_op[0]),
                     .core2mem_start_txn(core2mem_start_txn[0]),
                     .system_halt       (system_halt[0]),
                     .mmio_data_out     (mmio_data_out[0]),
                     .mmio_acknowledged (mmio_acknowledged[0]),
                     // Inouts
                     .core2mem_HIT      (core2mem_HIT),
                     .core2mem_HITM     (core2mem_HITM),
                     .core2mem_HITM_data(core2mem_HITM_data[31:0]),
                     // Inputs
                     .clock             (clock),
                     .reset             (reset),
                     .mem2core_data     (mem2core_data[31:0]),
                     .mem2core_done     (mem2core_done[0]),
                     .bus2core_writeback_done(bus2core_writeback_done),
                     .mem_bus_winner_address(mem_bus_winner_address[31:0]),
                     .mem_bus_winner_op (mem_bus_winner_op),
                     .mmio_address      (mmio_address[0]),
                     .mmio_data_in      (mmio_data_in[0]),
                     .mmio_write        (mmio_write[0]),
                     .core_no (0),
                     .core_initial_state(1));

    pipeline pipe_inst1(/*AUTOINST*/
                     // Outputs
                     .core2mem_address  (core2mem_address[1]),
                     .core2mem_data     (core2mem_data[1]),
                     .core2mem_bus_op   (core2mem_bus_op[1]),
                     .core2mem_start_txn(core2mem_start_txn[1]),
                     .system_halt       (system_halt[1]),
                     .mmio_data_out     (mmio_data_out[1]),
                     .mmio_acknowledged (mmio_acknowledged[1]),
                     // Inouts
                     .core2mem_HIT      (core2mem_HIT),
                     .core2mem_HITM     (core2mem_HITM),
                     .core2mem_HITM_data(core2mem_HITM_data[31:0]),
                     // Inputs
                     .clock             (clock),
                     .reset             (reset),
                     .mem2core_data     (mem2core_data[31:0]),
                     .mem2core_done     (mem2core_done[1]),
                     .bus2core_writeback_done(bus2core_writeback_done),
                     .mem_bus_winner_address(mem_bus_winner_address[31:0]),
                     .mem_bus_winner_op (mem_bus_winner_op),
                     .mmio_address      (mmio_address[1]),
                     .mmio_data_in      (mmio_data_in[1]),
                     .mmio_write        (mmio_write[1]),
                     .core_no (1),
                     .core_initial_state(0));

  
  integer               i;
  int                   k;
  integer               file;
  integer               temp;
  int                   showing_data;
  int                   j;
  
initial
  begin
    clock = 0;
    clocks = 0;
    reset = 1;
    mem_reset = 1;
//    $monitor("Time %d, PC: %d, clock %b system_halt %b", $time, pipe_inst.PC, clock, system_halt);

    @(posedge clock);
    @(posedge clock);
    mem_reset = 0;
    $display("Starting to load the memory");
    file = 0;
    file = $fopen("program.mem", "r");
    $display("Got fd %d", file);
    if (file == 0) begin
      $display("We failed to load the memory.");
      $finish;
    end


    i = 0;
    while (!$feof(file)) begin
      if ($fscanf(file, "%d", temp)) begin
        mem_inst.memory[i] = temp;
      end
      $display("memory[%d] : %d", i, mem_inst.memory[i]);
      i = i + 1;
    end

    @(posedge clock);
    reset = 0;

    $display("Deasserting reset");

    $display("Enabling core 0");
    
    $display("Beginning simulation");

    @(posedge clock);
    @(posedge clock);

    while(system_halt[1:0] != 2'b11) begin
      #10;
    end

    @(negedge clock);
    $display("\n\n\n@@@System HALTED on halt instruction\n\n");

    $display("Final contents of register file:");
    

    for (i = 0; i < 8; i++) begin
        $display("reg[%d] = %d \t %d", i, pipe_inst0.id_inst.registers_next[i], pipe_inst1.id_inst.registers_next[i]);
    end
    
    begin
      $display("@@@ Unified Memory contents hex on left, decimal on right: ");
      $display("@@@");
      showing_data = 0;
      for(k=0;k<=16383; k=k+1) begin
        int test;
        test = 0;
        temp = mem_inst.memory[k];
        for (j = 0; j < 32; j++) begin
          if (pipe_inst0.dcache_inst.lines_next[j].tag == k &&
              pipe_inst0.dcache_inst.lines_next[j].state == MODIFIED) begin
            temp = pipe_inst0.dcache_inst.lines_next[j].line;
            test++;
          end
        end
        for (j = 0; j < 32; j++) begin
          if (pipe_inst1.dcache_inst.lines_next[j].tag == k &&
              pipe_inst1.dcache_inst.lines_next[j].state == MODIFIED) begin
            temp = pipe_inst1.dcache_inst.lines_next[j].line;
            test++;
          end
        end

        assert(test < 2) else begin
          $display("Assertion failure: Line %d was found in %d caches in the modified state!", k, test);
          test = 0;
        end
        
          
        if (temp != 0) begin
          $display("@@@ mem[%5d] = %x : %0d", k, temp, temp);
          showing_data = 1;
        end else if (showing_data != 0) begin
          $display("@@@");
          showing_data = 0;
        end
      end
      $display("@@@");
    end
    
    $finish;
      
   
  end // initial begin

  always_ff @(posedge clock) begin
    $display("POSEDGE");
  end

  always @(negedge clock) begin
    $display("NEGEDGE");
  end

  
  always @(negedge clock) begin
    if (!reset) begin
      $display("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      $display("Time is %d\n\n", $time);

      if (pipe_inst0.squash) begin
        $display("@!@!@!@!@! BRANCH MISPREDICTION ON CORE 0 @!@!@!@!@!@!@!@");
      end
      if (pipe_inst1.squash) begin
        $display("@!@!@!@!@! BRANCH MISPREDICTION ON CORE 1 @!@!@!@!@!@!@!@");
      end

      
      
      $display("=========STALLING SIGNALS=========");
      $display("id_stall: core0: %12d\t core1: %d",
               pipe_inst0.id_stall, pipe_inst1.id_stall);
      $display("ex_stall: core0: %12d\t core1: %d",
               pipe_inst0.ex_stall, pipe_inst1.ex_stall);
      $display("mem_stall: core0: %12d\t core1: %d",
               pipe_inst0.mem_stall, pipe_inst1.mem_stall);
      
      $display("-----------IF STAGE--------------");
      if (pipe_inst0.if_instruction_valid || pipe_inst1.if_instruction_valid) begin
        $display("if_PC:                core0: %12d\t core1: %d",
                 pipe_inst0.if_PC, pipe_inst1.if_PC);
        $display("if_instruction:       core0: %12d\t core1: %d",
                 pipe_inst0.if_instruction, pipe_inst1.if_instruction);
        $display("if_instruction_valid: core0: %12d\t core1: %d",
                 pipe_inst0.if_instruction_valid, pipe_inst1.if_instruction_valid);
      end else begin
        $display("IDLE");
      end // else: !if(pipe_inst.if_instruction_valid)
      
      $display("-----------ID STAGE--------------");
      if (pipe_inst0.id_valid || pipe_inst1.id_valid) begin
        $display("id_PC             core0: %12d\t core1: %d",
                 pipe_inst0.id_PC, pipe_inst1.id_PC);
        $display("id_instruction    core0: %12d\t core1: %d", 
                 pipe_inst0.id_instruction, pipe_inst1.id_instruction);
        $display("id_opcode:        core0: %12d\t core1: %d", 
                 pipe_inst0.id_opcode, pipe_inst1.id_opcode);
        $display("id_reg_a_idx:     core0: %12d\t core1: %d", 
                 pipe_inst0.id_reg_a_idx, pipe_inst1.id_reg_a_idx);
        $display("id_reg_a_value:   core0: %12d\t core1: %d", 
                 pipe_inst0.id_reg_a_value, pipe_inst1.id_reg_a_value);
        $display("id_reg_a_check:   core0: %12d\t core1: %d", 
                 pipe_inst0.id_reg_a_check, pipe_inst1.id_reg_a_check);
        $display("id_reg_b_idx:     core0: %12d\t core1: %d",
                 pipe_inst0.id_reg_b_idx, pipe_inst1.id_reg_b_idx);
        $display("id_reg_b_value:   core0: %12d\t core1: %d", 
                 pipe_inst0.id_reg_b_value, pipe_inst1.id_reg_b_value);
        $display("id_reg_b_check:   core0: %12d\t core1: %d", 
                 pipe_inst0.id_reg_b_check, pipe_inst1.id_reg_b_check);
        $display("id_offset_field:  core0: %12d\t core1: %d", 
                 pipe_inst0.id_offset_field, pipe_inst1.id_offset_field);
        $display("id_dest_reg:      core0: %12d\t core1: %d", 
                 pipe_inst0.id_dest_reg, pipe_inst1.id_dest_reg);
        $display("id_dest_reg_used: core0: %12d\t core1: %d",
                 pipe_inst0.id_dest_reg_used, pipe_inst1.id_dest_reg_used);
        $display("id_valid:         core0: %12d\t core1: %d", 
                 pipe_inst0.id_valid, pipe_inst1.id_valid);
      end else begin // if (pipe_inst.id_valid)
        $display("IDLE");
      end // else: !if(pipe_inst.id_valid)
      
      $display("-----------EX STAGE--------------");
      if(pipe_inst0.ex_valid || pipe_inst1.ex_valid) begin
        $display("ex_PC:            core0: %12d\t core1: %d", 
                 pipe_inst0.ex_PC, pipe_inst1.ex_PC);
        $display("ex_instruction:   core0: %12d\t core1: %d",
                 pipe_inst0.ex_instruction, pipe_inst1.ex_instruction);
        $display("ex_opcode:        core0: %12d\t core1: %d",
                 pipe_inst0.ex_opcode, pipe_inst1.ex_opcode);
        $display("ex_dest_reg:      core0: %12d\t core1: %d",
                 pipe_inst0.ex_dest_reg, pipe_inst1.ex_dest_reg);
        $display("ex_dest_reg_used: core0: %12d\t core1: %d",
                 pipe_inst0.ex_dest_reg_used,  pipe_inst1.ex_dest_reg_used);
        $display("ex_dest_value:    core0: %12d\t core1: %d",
                 pipe_inst0.ex_dest_value, pipe_inst1.ex_dest_value);
        $display("ex_address:       core0: %12d\t core1: %d",
                 pipe_inst0.ex_address, pipe_inst1.ex_address);
        $display("ex_branch_taken:  core0: %12d\t core1: %d",
                 pipe_inst0.ex_branch_taken, pipe_inst1.ex_branch_taken);
        $display("ex_valid:         core0: %12d\t core1: %d",
                 pipe_inst0.ex_valid, pipe_inst1.ex_valid);
      end else begin // if (pipe_inst.ex_valid)
        $display("IDLE");
      end // else: !if(pipe_inst.ex_valid)
      
      $display("-----------MEM STAGE--------------");
      if(pipe_inst0.mem_valid || pipe_inst1.mem_valid) begin
        $display("mem_PC:            core0: %12d\t core1: %d",
                 pipe_inst0.mem_PC, pipe_inst1.mem_PC);
        $display("mem_instruction:   core0: %12d\t core1: %d",
                 pipe_inst0.mem_instruction, pipe_inst1.mem_instruction);
        $display("mem_dest_reg:      core0: %12d\t core1: %d",
                 pipe_inst0.mem_dest_reg, pipe_inst1.mem_dest_reg);
        $display("mem_dest_reg_used: core0: %12d\t core1: %d",
                 pipe_inst0.mem_dest_reg_used, pipe_inst1.mem_dest_reg_used);
        $display("mem_dest_value:    core0: %12d\t core1: %d",
                 pipe_inst0.mem_dest_value, pipe_inst1.mem_dest_value);
        $display("mem_valid:         core0: %12d\t core1: %d",
                 pipe_inst0.mem_valid, pipe_inst1.mem_valid);
        $display("branch_PC_next:    core0: %12d\t core1: %d",
                 pipe_inst0.branch_PC_next, pipe_inst1.branch_PC_next);
        $display("branch_taken:      core0: %12d\t core1: %d",
                 pipe_inst0.branch_taken, pipe_inst1.branch_taken);
      end else begin // if (pipe_inst.mem_valid)
        $display("IDLE");
      end // else: !if(pipe_inst.mem_valid)
      $display("-----------WB STAGE--------------");
      if(pipe_inst0.wb_dest_reg_used || pipe_inst1.wb_dest_reg_used) begin
        $display("wb_PC:            core0: %12d\t core1: %d",
                 pipe_inst0.wb_PC, pipe_inst1.wb_PC);
        $display("wb_instruction:   core0: %12d\t core1: %d",
                 pipe_inst0.wb_instruction, pipe_inst1.wb_instruction);
        $display("wb_dest_reg:      core0: %12d\t core1: %d",
                 pipe_inst0.wb_dest_reg, pipe_inst1.wb_dest_reg);
        $display("wb_dest_reg_used: core0: %12d\t core1: %d",
                 pipe_inst0.wb_dest_reg_used, pipe_inst1.wb_dest_reg_used);
        $display("wb_dest_value:    core0: %12d\t core1: %d",
                 pipe_inst0.wb_dest_value, pipe_inst1.wb_dest_value);
      end else begin
        $display("IDLE");
      end // else: !if(pipe_inst0.wb_dest_reg_used || pipe_inst1.wb_dest_reg_used)

      $display("+++++ %d: [%d] %d <- %d     | [%d] %d <- %d",
               clocks, pipe_inst0.wb_PC, pipe_inst0.wb_dest_reg_used ? pipe_inst0.wb_dest_reg : -1,
               pipe_inst0.wb_dest_value, pipe_inst1.wb_PC,
               pipe_inst1.wb_dest_reg_used ? pipe_inst1.wb_dest_reg : -1,
               pipe_inst1.wb_dest_value);
      
      $display("$$$$$$$$$$$$$$ CORE 0 CACHES");
      $display("dcache2core_done %b \t dcache2core_data %d", pipe_inst0.dcache2core_done, pipe_inst0.dcache2core_data);
      $display("icache2core_valid %b \t icache2core_data %d", pipe_inst0.icache2core_valid, pipe_inst0.icache2core_data);
      $display("PC %d \t icache2mem_start_txn", pipe_inst0.PC, pipe_inst0.icache2mem_start_txn);

      $display("$$$$$$$$$$$$$$ CORE 1 CACHES");
      $display("dcache2core_done %b \t dcache2core_data %d", pipe_inst1.dcache2core_done, pipe_inst1.dcache2core_data);
      $display("icache2core_valid %b \t icache2core_data %d", pipe_inst1.icache2core_valid, pipe_inst1.icache2core_data);
      $display("PC %d \t icache2mem_start_txn", pipe_inst1.PC, pipe_inst1.icache2mem_start_txn);


      $display("-------------MEMORY BUS------------------");
      for (i = 0; i < 4; i++) begin
        $display("core2mem_address[%d] %d \t core2mem_data[%d] %d \t core2mem_bus_op[%d] %s, core2mem_start_txn[%d] %d", i, core2mem_address[i], i, core2mem_data[i], i, core2mem_bus_op[i], i, core2mem_start_txn[i]);
      end

      $display("mem2core_data %x | %d", mem2core_data, mem2core_data);
      for (i = 0; i < 4; i++) begin
        $display("mem2core_done[%d] %d", i, mem2core_done[i]);
      end

      $display("HIT: %d \t HITM %d \t HITM_data %d", core2mem_HIT, core2mem_HITM, core2mem_HITM_data);

      $display("mem_bus_winner_address %d \t mem_bus_winner_op %s", mem_bus_winner_address, mem_bus_winner_op);

      $display("mem2bus_data %d \t mem2bus_done %d", mem2bus_data, mem2bus_done);
      $display("bus2mem_address %d \t bus2mem_data %d \t bus2mem_write %d \t bus2mem_start_txn", bus2mem_address, bus2mem_data, bus2mem_write, bus2mem_start_txn);
      
      $display("\n\nRegisters\n");
      for (i = 0; i < 8; i++) begin
        $display("reg[%d] = %d \t %d", i, pipe_inst0.id_inst.registers_next[i], pipe_inst1.id_inst.registers_next[i]);
      end


      $display("\n\n\n");
    end // if (!reset)
  end
  
  
endmodule // testbench


// Local Variables:
// verilog-library-flags:("-y verilog/")
// End:
